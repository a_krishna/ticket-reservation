package com.myticket.core

object ObjectId {
  def apply(): String = new org.bson.types.ObjectId().toString
}
