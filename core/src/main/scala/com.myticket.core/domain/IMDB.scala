package com.myticket.core.domain

import com.myticket.core.ObjectId
import com.myticket.core.service.ValidationError
import com.myticket.core.service.ValidationService._
import org.joda.time.{ DateTime, DateTimeZone }
import com.novus.salat.annotations.Key
import com.novus.salat.annotations.raw.Salat
import play.api.libs.json._

import scalaz.Failure

case class IMDB(@Key("_id") id: String = ObjectId(),
  movieName: String,
  releasedDate: Option[Long] = None,
  budget: Option[Double] = None,
  boxOffice: Option[Double] = None,
  rating: Option[Long] = None,
  reviews: List[String] = List.empty,
  createdOn: Long = DateTime.now(DateTimeZone.UTC).getMillis,
  updatedOn: Option[Long] = None,
  casts: Map[String, String] = Map.empty)
object IMDB {
  implicit val imdb = Format(Json.reads[IMDB], Json.writes[IMDB])

  implicit val validator: Validator[IMDB] = imdb ⇒ {
    import imdb._

    createValidated(success = imdb, errors = PartialIMDB.validationErrors(Some(movieName)))
  }
}

case class PartialIMDB(movieName: Option[String] = None,
  releasedDate: Option[Long] = None,
  budget: Option[Double] = None,
  boxOffice: Option[Double] = None,
  rating: Option[Long] = None,
  reviews: Option[List[String]] = None,
  createdOn: Option[Long] = None,
  updatedOn: Option[Long] = None,
  casts: Option[Map[String, String]] = None)

object PartialIMDB {

  implicit val imdb = Format(Json.reads[PartialIMDB], Json.writes[PartialIMDB])

  implicit val validator: Validator[PartialIMDB] = partialIMDB ⇒ {
    import partialIMDB._

    createValidated(success = partialIMDB, errors = validationErrors(movieName))
  }
  def validationErrors(movieName: Option[String]) = {
    import scalaz.std.option._
    import scalaz.syntax.traverse._

    coalesceValidationErrors(
      movieName.traverse[Validated, String](checkNonEmptyString(_, ValidationError("MovieNameRequired"))))
  }
}
object TimeService {
  def beginningOfDay(time: Long) = new DateTime(time, DateTimeZone.UTC).withTimeAtStartOfDay().getMillis
  def endOfTheDay(time: Long) = new DateTime(time, DateTimeZone.UTC).withTime(21, 0, 0, 0).getMillis
}
case class Theatre(@Key("_id") id: String = ObjectId(),
    name: String,
    city: String,
    state: String,
    country: String,
    screenIds: Set[String] = Set.empty,
    rating: Option[Long] = None,
    reviewIds: List[String] = List.empty,
    createdOn: Long = DateTime.now(DateTimeZone.UTC).getMillis,
    updatedOn: Option[Long] = None) {

  def copyPartial(pT: PartialTheatre): Theatre = copy(
    name = pT.name.getOrElse(name),
    city = pT.city.getOrElse(city),
    state = pT.state.getOrElse(state),
    country = pT.country.getOrElse(country),
    screenIds = pT.screenIds.getOrElse(screenIds),
    reviewIds = pT.reviewIds.getOrElse(reviewIds),
    rating = pT.rating)

}

object Theatre {
  implicit val theatre = Format(Json.reads[Theatre], Json.writes[Theatre])

  implicit val validator: Validator[Theatre] = theatre ⇒ {
    import theatre._
    createValidated(success = theatre, errors = PartialTheatre.validationErrors(Some(name), Some(city), Some(state), Some(country)))
  }
}

case class PartialTheatre(name: Option[String] = None,
  city: Option[String] = None,
  state: Option[String] = None,
  country: Option[String] = None,
  screenIds: Option[Set[String]] = None,
  rating: Option[Long] = None,
  reviewIds: Option[List[String]] = None,
  createdOn: Option[Long] = None,
  updatedOn: Option[Long] = None)
object PartialTheatre {
  implicit val pT = Format(Json.reads[PartialTheatre], Json.writes[PartialTheatre])

  implicit val validator: Validator[PartialTheatre] = partialTheatre ⇒ {
    import partialTheatre._
    createValidated(success = partialTheatre, errors = validationErrors(name, city, state, country))
  }

  def validationErrors(name: Option[String], city: Option[String], state: Option[String], country: Option[String]) = {
    import scalaz.std.option._
    import scalaz.syntax.traverse._

    coalesceValidationErrors(
      name.traverse[Validated, String](checkNonEmptyString(_, ValidationError("NameRequired"))),
      city.traverse[Validated, String](checkNonEmptyString(_, ValidationError("CityNameRequired"))),
      state.traverse[Validated, String](checkNonEmptyString(_, ValidationError("StateNameRequired"))),
      country.traverse[Validated, String](checkNonEmptyString(_, ValidationError("CountryNameRequired"))))
  }

  def copyTheatre(pt: PartialTheatre) = Theatre(
    name = pt.name.get,
    city = pt.city.get,
    state = pt.state.get,
    country = pt.country.get).copyPartial(pt)
}

case class Seat(seatName: String, classType: String, price: Double) extends Ordered[Seat] {
  override def compare(that: Seat): Int = this.seatName compare that.seatName
}

object Seat {
  implicit val seats = Format(Json.reads[Seat], Json.writes[Seat])
}

@Salat sealed trait ScreenType {
  def price: Double
}
case object IMAX extends ScreenType {
  def price: Double = 250
}

case object `3D` extends ScreenType {
  def price: Double = 100
}

case object AURO3D extends ScreenType {
  def price: Double = 200
}

case object `2D` extends ScreenType {
  def price: Double = 0.0
}

object ScreenType {
  implicit val formatter: Format[ScreenType] = new Format[ScreenType] {
    def reads(json: JsValue) = json match {
      case JsString("imax")   ⇒ JsSuccess(IMAX)
      case JsString("3d")     ⇒ JsSuccess(`3D`)
      case JsString("2d")     ⇒ JsSuccess(`2D`)
      case JsString("auro3d") ⇒ JsSuccess(AURO3D)
      case other              ⇒ JsError(s"Unexpected value '$other'")
    }

    def writes(o: ScreenType): JsValue = o match {
      case IMAX   ⇒ JsString("imax")
      case `3D`   ⇒ JsString("3d")
      case `2D`   ⇒ JsString("2d")
      case AURO3D ⇒ JsString("auro3d")
    }
  }
}

case class Screen(@Key("_id") id: String = ObjectId(),
    screenName: String,
    screenType: ScreenType = `2D`,
    seatsId: Set[Seat] = Set.empty[Seat],
    capacity: Option[Long] = None,
    priceAddOn: Double = `2D`.price,
    layout: Option[String] = None,
    createdOn: Long = DateTime.now(DateTimeZone.UTC).getMillis,
    updatedOn: Option[Long] = None) extends Ordered[Screen] {

  override def compare(that: Screen): Int = this.screenName compare that.screenName
  def copyPartial(ps: PartialScreen) = {
    copy(seatsId = ps.seatsId.getOrElse(seatsId),
      capacity = ps.capacity,
      layout = ps.layout,
      priceAddOn = screenType.price)
  }
}

object Screen {

  import Seat._

  implicit val screen = Format(Json.reads[Screen], Json.writes[Screen])

  implicit val validator: Validator[Screen] = screen ⇒ {
    import screen._
    createValidated(success = screen, errors = PartialScreen.validationErrors(Some(screenName), capacity))
  }
}

case class PartialScreen(screenName: Option[String] = None,
  screenType: Option[ScreenType] = None,
  seatsId: Option[Set[Seat]] = None,
  capacity: Option[Long] = None,
  priceAddOn: Option[Double] = None,
  layout: Option[String] = None,
  createdOn: Option[Long] = None,
  updatedOn: Option[Long] = None)

object PartialScreen {
  import Seat._

  implicit val pS = Format(Json.reads[PartialScreen], Json.writes[PartialScreen])
  implicit val validator: Validator[PartialScreen] = partialScreen ⇒ {
    import partialScreen._
    createValidated(success = partialScreen, errors = validationErrors(screenName, capacity))
  }

  def validationErrors(screenName: Option[String], capacity: Option[Long]) = {
    import scalaz.std.option._
    import scalaz.syntax.traverse._

    coalesceValidationErrors(
      screenName.traverse[Validated, String](checkNonEmptyString(_, ValidationError("ScreenNameRequired"))),
      capacity.traverse[Validated, Long](checkPositiveLong(_, ValidationError("CapacityRequired"))))
  }

  def copyScreen(pS: PartialScreen) = Screen(screenName = pS.screenName.get, screenType = pS.screenType.get).copyPartial(pS)
}

case class Show(@Key("_id") id: String = ObjectId(),
  imdbId: String,
  theatreId: String,
  screenId: String,
  time: Long,
  availableSeatsId: Set[Seat] = Set.empty,
  reservedSeatsId: Set[Seat] = Set.empty,
  createdOn: Long = DateTime.now(DateTimeZone.UTC).getMillis,
  updatedOn: Option[Long] = None)
object Show {

  implicit val format = Format(Json.reads[Show], Json.writes[Show])

  implicit val validator: Validator[Show] = show ⇒ {
    import show._
    createValidated(success = show, errors = PartialShow.validationErrors(Some(imdbId), Some(theatreId), Some(screenId), Some(time)))
  }
}

case class PartialShow(imdbId: Option[String] = None,
  theatreId: Option[String] = None,
  screenId: Option[String] = None,
  time: Option[Long] = None,
  availableSeatsId: Option[Set[Seat]] = None,
  reservedSeatsId: Option[Set[Seat]] = None,
  createdOn: Option[Long] = None,
  updatedOn: Option[Long] = None)
object PartialShow {
  implicit val format = Format(Json.reads[PartialShow], Json.writes[PartialShow])

  implicit val validator: Validator[PartialShow] = partialShow ⇒ {
    import partialShow._
    createValidated(success = partialShow, errors = validationErrors(imdbId, theatreId, screenId, time))
  }

  def validationErrors(imdbId: Option[String], theatreId: Option[String], screenId: Option[String], time: Option[Long]) = {
    import scalaz.std.option._
    import scalaz.syntax.traverse._

    coalesceValidationErrors(
      imdbId.traverse[Validated, String](checkNonEmptyString(_, ValidationError("MovieRequired"))),
      theatreId.traverse[Validated, String](checkNonEmptyString(_, ValidationError("MovieRequired"))),
      screenId.traverse[Validated, String](checkNonEmptyString(_, ValidationError("ScreenRequired"))),
      time.traverse[Validated, Long](checkPositiveLong(_, ValidationError("TimeOfShowRequired"))))
  }
}

@Salat sealed trait OrderStatus
case object Booked extends OrderStatus
case object Pending extends OrderStatus
case object Rejected extends OrderStatus
case object NotAvailable extends OrderStatus
case object Cancel extends OrderStatus

object OrderStatus {
  implicit val formatter: Format[OrderStatus] = new Format[OrderStatus] {
    def reads(json: JsValue) = json match {
      case JsString("booked")       ⇒ JsSuccess(Booked)
      case JsString("pending")      ⇒ JsSuccess(Pending)
      case JsString("rejected")     ⇒ JsSuccess(Rejected)
      case JsString("notavailable") ⇒ JsSuccess(NotAvailable)
      case JsString("cancel")       ⇒ JsSuccess(Cancel)
      case other                    ⇒ JsError(s"Unexpected value '$other'")
    }

    def writes(o: OrderStatus): JsValue = o match {
      case Booked       ⇒ JsString("booked")
      case Pending      ⇒ JsString("pending")
      case Rejected     ⇒ JsString("rejected")
      case NotAvailable ⇒ JsString("notavailable")
      case Cancel       ⇒ JsString("cancel")
    }
  }
}

case class Order(@Key("_id") id: String = ObjectId(),
  showId: String,
  userId: String,
  seats: Set[Seat] = Set.empty,
  cardId: String,
  totalPrice: Double,
  ticketId: Option[String] = None,
  status: Option[OrderStatus] = None,
  createdOn: Long = DateTime.now(DateTimeZone.UTC).getMillis,
  updatedOn: Option[Long] = None)

object Order {
  implicit val format = Format(Json.reads[Order], Json.writes[Order])
}

case class PartialOrder(showId: Option[String] = None,
  userId: Option[String] = None,
  seatId: Option[Set[Seat]] = None,
  cardId: Option[String] = None,
  ticketId: Option[String] = None,
  status: Option[OrderStatus] = None,
  totalPrice: Option[Double] = None,
  createdOn: Option[Long] = None,
  updatedOn: Option[Long] = None)

object PartialOrder {
  implicit val format = Format(Json.reads[PartialOrder], Json.writes[PartialOrder])
}
