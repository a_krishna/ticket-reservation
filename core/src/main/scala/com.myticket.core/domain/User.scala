package com.myticket.core.domain

import com.myticket.core.ObjectId
import com.myticket.core.service.ValidationService._
import com.myticket.core.service.{ PasswordService, ValidationError }
import com.novus.salat.annotations.Key
import com.novus.salat.annotations.raw.Salat
import org.joda.time.{ DateTime, DateTimeZone }
import play.api.libs.json._

import scalaz.syntax.validation._

@Salat sealed trait UserStatus
case object ActiveUser extends UserStatus
case object InActiveUser extends UserStatus

object UserStatus {

  implicit val formatter: Format[UserStatus] = new Format[UserStatus] {
    def reads(json: JsValue) = json match {
      case JsString("active")   ⇒ JsSuccess(ActiveUser)
      case JsString("inactive") ⇒ JsSuccess(InActiveUser)
      case other                ⇒ JsError(s"Unexpected value '$other'")
    }

    def writes(o: UserStatus): JsValue = o match {
      case ActiveUser   ⇒ JsString("active")
      case InActiveUser ⇒ JsString("inactive")
    }
  }

}

@Salat sealed trait Role
case object Admin extends Role
case object Anonymous extends Role
case object NormalUser extends Role

object Role {

  implicit val formatter: Format[Role] = new Format[Role] {
    def reads(json: JsValue) = json match {
      case JsString("admin")     ⇒ JsSuccess(Admin)
      case JsString("anonymous") ⇒ JsSuccess(Anonymous)
      case JsString("user")      ⇒ JsSuccess(NormalUser)
      case other                 ⇒ JsError(s"Unexpected value '$other'")
    }

    def writes(o: Role): JsValue = o match {
      case Admin      ⇒ JsString("admin")
      case Anonymous  ⇒ JsString("anonymous")
      case NormalUser ⇒ JsString("user")
    }
  }

}

case class User(@Key("_id") id: String = ObjectId(),
  name: String,
  hashedPassword: String,
  email: Option[String] = None,
  role: Role = NormalUser,
  salt: String,
  firstName: String,
  lastName: String,
  watchList: List[String] = List.empty,
  wishList: List[String] = List.empty,
  defaultCardId: Option[String] = None,
  cardIds: List[String] = List.empty,
  createdOn: Long = DateTime.now(DateTimeZone.UTC).getMillis,
  updatedOn: Option[Long] = None,
  status: UserStatus = ActiveUser)

object User {
  //Any user json that is output from this process will have sensitive data properly redacted by this formatter's writer
  implicit val formatter = Format(Json.reads[User], Json.writes[User].transform(_.as[JsObject] ++ Json.obj("hashedPassword" -> "*****", "salt" -> "*****")))

  //Reads 'New User' json, which is 'User' json w/ an additional 'password' field, and w/o 'hashedPassword' and 'salt'
  //Then, using that 'password' field, generates the proper 'User' object with 'hashedPassword', a new 'salt', etc.
  //This is used by an endpoint that needs to generate new users. Everywhere else we use the normal reader
  val credentialHashingReader = Json.reads[User].compose(Reads.JsObjectReads.flatMap { newUserJsObject ⇒
    (JsPath \ "password").read[String].map { password ⇒ //We already validated "password" field w/ "validatePassword" directive
      val salt = PasswordService.newSalt
      val hashedPassword = PasswordService.hashedPassword(password, salt)
      val generatedUserFields = Json.obj("id" -> ObjectId(), "hashedPassword" -> hashedPassword, "salt" -> salt, "createdOn" -> DateTime.now().getMillis)

      //Combine read json and generated json into a user
      newUserJsObject ++ generatedUserFields
    }
  })

  implicit val validator: Validator[User] = user ⇒ {
    import user._

    val validationErrors = PartialUser.validationErrors(Some(name), Some(firstName), Some(lastName), email)
    createValidated(success = user, errors = validationErrors)
  }
}

case class PartialUser(name: Option[String] = None,
  hashedPassword: Option[String] = None,
  email: Option[String] = None,
  role: Option[Role] = None,
  salt: Option[String] = None,
  firstName: Option[String] = None,
  lastName: Option[String] = None,
  watchList: Option[List[String]] = None,
  wishList: Option[List[String]] = None,
  defaultCardId: Option[String] = None,
  cardIds: Option[List[String]] = None,
  createdOn: Option[Long] = None,
  updatedOn: Option[Long] = None,
  status: Option[UserStatus] = None)

object PartialUser {
  implicit val formatter = Format(Json.reads[PartialUser], Json.writes[PartialUser])

  implicit val validator: Validator[PartialUser] = partialUser ⇒ {
    import partialUser._

    createValidated(success = partialUser, errors = validationErrors(name, firstName, lastName, email))
  }

  def validationErrors(username: Option[String], firstName: Option[String], lastName: Option[String], email: Option[String]) = {
    import scalaz.std.option._
    import scalaz.syntax.traverse._

    coalesceValidationErrors(
      username.traverse[Validated, String](Credentials.checkUsername),
      firstName.traverse[Validated, String](checkNonEmptyString(_, ValidationError("UserFirstNameRequired"))),
      lastName.traverse[Validated, String](checkNonEmptyString(_, ValidationError("UserLastNameRequired"))),
      email.traverse[Validated, String](checkNonEmptyString(_, ValidationError("UserEmailRequired"))))
  }
}

case class Credentials(
  userName: String,
  password: String)

object Credentials {
  implicit val reader = Json.reads[Credentials] //Read but never write because it contains sensitive data (i.e. password)

  implicit val validator: Validator[Credentials] = credentials ⇒ {
    import credentials._

    val validationErrors = coalesceValidationErrors(checkUsername(userName), checkPassword(password))

    createValidated(success = credentials, errors = validationErrors)
  }

  def checkUsername(username: String): Validated[String] =
    username.successNel //TODO: write an actual validation

  def checkPassword(password: String): Validated[String] =
    if (PasswordService.isValid(password)) password.successNel else ValidationError("UserPasswordInvalid").failureNel

}

@Salat sealed trait CardType
case object Debit extends CardType
case object Credit extends CardType
case object Wallet extends CardType
//case class EWallet(name:String) extends CardType

object CardType {
  implicit val formatter: Format[CardType] = new Format[CardType] {
    def reads(json: JsValue) = json match {
      case JsString("debit")  ⇒ JsSuccess(Debit)
      case JsString("credit") ⇒ JsSuccess(Credit)
      case JsString("wallet") ⇒ JsSuccess(Wallet)
      case other              ⇒ JsError(s"Unexpected value '$other'")
    }

    def writes(o: CardType): JsValue = o match {
      case Debit  ⇒ JsString("debit")
      case Credit ⇒ JsString("credit")
      case Wallet ⇒ JsString("wallet")
    }
  }
}

case class Card(@Key("_id") id: String = ObjectId(),
  name: String,
  cardType: CardType = Wallet,
  credit: Double,
  provider: Option[String] = None,
  expiryDate: Option[Long] = None,
  createdOn: Long = DateTime.now(DateTimeZone.UTC).getMillis,
  updatedOn: Option[Long] = None)

object Card {
  implicit val validator: Validator[Card] = card ⇒ {
    import card._
    val validationErrors = PartialCard.validationErrors(Some(name), provider, expiryDate)
    createValidated(success = card, errors = validationErrors)
  }

}

case class PartialCard(name: Option[String] = None,
  cardType: Option[CardType] = None,
  credit: Option[Number] = None,
  provider: Option[String] = None,
  expiryDate: Option[Long] = None,
  createdOn: Option[Long] = None,
  updatedOn: Option[Long] = None)

object PartialCard {
  implicit val validator: Validator[PartialCard] = partialCard ⇒ {
    import partialCard._
    createValidated(success = partialCard, errors = validationErrors(name, provider, expiryDate))
  }
  def validationErrors(name: Option[String], provider: Option[String], expiryDate: Option[Long]) = {
    import scalaz.std.option._
    import scalaz.syntax.traverse._

    coalesceValidationErrors(
      name.traverse[Validated, String](Credentials.checkUsername),
      provider.traverse[Validated, String](checkNonEmptyString(_, ValidationError("ProviderRequired"))),
      expiryDate.traverse[Validated, Long](checkPositiveLong(_, ValidationError("ExpiryDateRequired"))))
  }
}

case class Review(@Key("_id") id: String = ObjectId(),
  message: String,
  fromId: Option[String] = None,
  toId: Option[String] = None,
  createdOn: Long = DateTime.now(DateTimeZone.UTC).getMillis,
  updateOn: Option[Long] = None)

object Review {
  implicit val validator: Validator[Review] = review ⇒ {
    import review._
    val validationError = PartialReview.validationErrors(Some(message), fromId, toId)
    createValidated(success = review, errors = validationError)
  }
}

case class PartialReview(message: Option[String],
  fromId: Option[String] = None,
  toId: Option[String] = None,
  createdOn: Option[Long] = None,
  updateOn: Option[Long] = None)

object PartialReview {

  implicit val validator: Validator[PartialReview] = partialReview ⇒ {
    import partialReview._
    createValidated(success = partialReview, errors = validationErrors(message, fromId, toId))
  }
  def validationErrors(message: Option[String], fromId: Option[String], toId: Option[String]) = {
    import scalaz.std.option._
    import scalaz.syntax.traverse._

    coalesceValidationErrors(
      message.traverse[Validated, String](checkMinimumStringLength(_, 40, ValidationError("Minimum 40 Character Required"))),
      fromId.traverse[Validated, String](checkNonEmptyString(_, ValidationError("UserRequired"))),
      toId.traverse[Validated, String](checkNonEmptyString(_, ValidationError("ProviderRequired"))))
  }
}

case class Transcation(@Key("_id") id: String = ObjectId(),
  fromId: String,
  toId: String,
  amount: Double,
  createdOn: Long = DateTime.now(DateTimeZone.UTC).getMillis)