package com.myticket.core.service


import java.util.UUID

import com.myticket.core.service.ValidationService._
import org.joda.time.{DateTime, DateTimeZone}
import play.api.libs.json.{JsString, JsValue, Writes}

import scala.collection.immutable.Seq
import scalaz.Validation.FlatMap._
import scalaz.{Failure, NonEmptyList, Success, ValidationNel}
import scalaz.syntax.validation._
import com.myticket.core.service.ValidationError._

object ValidationService {

  @annotation.implicitNotFound(msg = "No validator defined for ${T}. Try placing one in ${T}'s companion object.")
  type Validator[T] = T => Validated[T]
  type Validated[T] = ValidationNel[ValidationError, T]

  def validate[T](validatable: T)(implicit validator: Validator[T]): Validated[T] = validator(validatable)


  def coalesceValidationErrors[T](validations: ValidationNel[ValidationError, _]*): Seq[ValidationError] =
    validations.toList.collect { case Failure(validationErrorNel) => validationErrorNel.list }.flatten

  def createValidated[T](success: T, errors: Seq[ValidationError]): ValidationNel[ValidationError, T] =
    errors match {
      case Nil => Success(success)
      case head :: tail => Failure(NonEmptyList(head, tail: _*))
    }

  def checkNonEmptyOption[U](option: Option[U], error: ValidationError = EmptyOption): Validated[U] =
    option.map(_.successNel).getOrElse(error.failureNel)

  def checkNonEmptyStringOpt(stringOpt: Option[String], error: ValidationError = EmptyString): Validated[String] =
    checkNonEmptyOption(stringOpt).flatMap(string => checkNonEmptyString(string, error))

  def checkNonEmpty[U <: Traversable[_]](traversable: U, error: ValidationError = Empty): Validated[U] =
    if (traversable.nonEmpty) traversable.successNel else error.failureNel

  def checkTiming(first: DateTime, second: Option[DateTime], error: ValidationError = TimeWarp): Validated[DateTime] =
    if (second.isEmpty || first.isBefore(second.get)) first.successNel else error.failureNel

  def checkTimingLong(time:Long, error: ValidationError = TimeWarp): Validated[Long] =
    if(getTime(time).isAfter(currentTime)) time.successNel else error.failureNel

  def checkTimingOpt(firstOpt: Option[DateTime], secondOpt: Option[DateTime], error: ValidationError = TimeWarp): Validated[Option[DateTime]] =
    (firstOpt, secondOpt) match {
      case (Some(first), Some(second)) if first isBefore second => firstOpt.successNel
      case (Some(first), None) => firstOpt.successNel
      case (None, None) => firstOpt.successNel
      case _ => error.failureNel
    }

  def checkMinimumStringLength(input: String, min: Int, error: ValidationError = StringLength): Validated[String] =
    checkNonEmptyString(input, error) flatMap {
      case s if s.trim.length < min => error.failureNel
      case _ => input.successNel
    }

  def checkNonEmptyString(string: String, error: ValidationError = EmptyString): Validated[String] =
    if (string != null && string.trim.length > 0) string.successNel else error.failureNel

  def checkUuid(uuid: UUID, error: ValidationError = NullUuid): Validated[UUID] =
    if (uuid != null) uuid.successNel else error.failureNel

  def checkPositiveNumber[U](number: U, error: ValidationError = NegativeNumber)(implicit numeric: Numeric[U]): Validated[U] =
    if (number != null && numeric.compare(number, numeric.zero) >= 0) number.successNel else error.failureNel

  def checkPositiveDouble(value: Double, error: ValidationError = NegativeNumber): Validated[Double] =
    if (!value.isNaN && value > 0.0) value.successNel else error.failureNel


  def checkPositiveLong(value: Long, error: ValidationError = NegativeNumber): Validated[Long] =
      if (value >0L) value.successNel else error.failureNel

  def checkOption[U](option: Option[U], error: ValidationError = EmptyOption): Validated[U] =
    option match {
      case Some(x) ⇒ x.successNel
      case _ ⇒ error.failureNel
    }

  def checkNonEmptyString(stringOpt: Option[String]): Validated[String] = checkOption(stringOpt).flatMap(checkNonEmptyString(_))

  def checkNonNull[U](traversable: U, error: ValidationError = Empty): Validated[U] =
    if (traversable != null) traversable.successNel else error.failureNel

  def getTime(time:Long)=new DateTime(time,DateTimeZone.UTC)
  def currentTime=DateTime.now(DateTimeZone.UTC).plusDays(2).getMillis

}

/**
  * Standard validation errors
  */
case class ValidationError(key: String, msg: Option[String] = None)

object ValidationError {

  val NullUuid = ValidationError("NullUuid")
  val EmptyString = ValidationError("EmptyString")
  val NegativeNumber = ValidationError("NegativeNumber")
  val EmptyOption = ValidationError("EmptyOption")
  val Empty = ValidationError("Empty")
  val TimeWarp = ValidationError("TimeWarp")
  val StringLength = ValidationError("StringLength")
  val DuplicateKey = ValidationError("DuplicateKey")

  implicit val writer = new Writes[ValidationError] {
    def writes(validationError: ValidationError): JsValue = JsString(validationError.key)
  }

}