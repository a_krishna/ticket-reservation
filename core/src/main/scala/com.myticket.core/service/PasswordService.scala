package com.myticket.core.service

import java.lang.Integer.parseInt
import java.security.SecureRandom
import java.util.UUID
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec

object PasswordService {

  def hashedPassword(password: String, salt: String): String = {
    //PBKDF2 with SHA-1 as the hashing algorithm. Note that the NIST
    //specifically names SHA-1 as an acceptable hashing algorithm for PBKDF2
    val algorithm = "PBKDF2WithHmacSHA1"

    //SHA-1 generates 160 bit hashes, so that's what makes sense here
    val derivedKeyLength = 160

    //Pick an iteration count that works for you. The NIST recommends at
    //least 1,000 iterations:
    //http://csrc.nist.gov/publications/nistpubs/800-132/nist-sp800-132.pdf
    val iterations = 1000

    val keySpec = new PBEKeySpec(password.toCharArray, hexToBytes(salt), iterations, derivedKeyLength)
    val secretKeyFactory = SecretKeyFactory.getInstance(algorithm)

    val secretKey = secretKeyFactory.generateSecret(keySpec).getEncoded
    bytesToHex(secretKey)
  }

  def newSalt: String = {
    val secureRandom = SecureRandom.getInstance("SHA1PRNG")
    val salt = new Array[Byte](8)

    secureRandom.nextBytes(salt)
    bytesToHex(salt)
  }

  def hexToBytes(hex: String): Array[Byte] = hex.grouped(2).map(parseInt(_, 16).toByte).toArray

  def bytesToHex(bytes: Array[Byte]): String = bytes.map("%02X" format _).mkString

  def isValid(password: String) = password.length >= 8

  val secret = UUID.randomUUID().toString + UUID.randomUUID().toString

}
