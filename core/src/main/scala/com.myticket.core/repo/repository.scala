package com.myticket.core.repo

import com.mongodb.casbah.Imports._
import com.novus.salat.dao.SalatDAO
import com.novus.salat.{ Context, grater }

import scala.collection.immutable.Seq
import scala.concurrent.stm.TMap
import scala.reflect.runtime.universe.typeOf

/**
 * A generic abstraction over all persistence layers.
 * A repository implementation MUST allow concurrent access (i.e. be thread-safe). In production, it is typically backed by a persistence store that
 * supports this through some mechanism like transactions. In tests, the repo can be mocked by either being stateless or being backed by some
 * type of concurrent hash map.
 * It's impractical to use method return types to model all possible error and success states of all the possible repository implementations.
 * Therefore, all method calls potentially throw exceptions. Make sure to handle them with Try's (routes and futures automatically do so)
 */
trait Repository {

  type Id <: AnyRef //Entity's unique identifier (e.g. UUID). May be a tuple or case class in the case of compound primary keys
  type Entity <: AnyRef //Business domain object with a unique identifier
  type PartialEntity <: AnyRef //Container of entity fields that need updating. Usually a case class mimicking the entity but with all optional fields

  def id(entity: Entity): Id //Extracts entity's id for usage in queries
  def get(id: Id): Option[Entity]
  def getByIds(id: Seq[Id]): Seq[Entity]
  def getAll: Seq[Entity]
  def insert(entity: Entity): Unit
  def insert(entities: Seq[Entity]): Unit
  def delete(id: Id): Unit
  def update(id: Id, partialEntity: PartialEntity): Unit
  def upsert(entity: Entity): UpsertStatus

}

sealed trait UpsertStatus
object UpsertStatus {
  case object Updated extends UpsertStatus
  case object Inserted extends UpsertStatus
}

class OptimisticLockFailureException extends RuntimeException

trait InMemoryRepository extends Repository {

  protected val map = TMap.empty[Id, Entity]

  def get(id: Id): Option[Entity] = map.single.get(id)
  def getAll: Seq[Entity] = map.single.values.toList
  def insert(entity: Entity): Unit = map.single.update(id(entity), entity)
  def insert(entities: Seq[Entity]): Unit = map.single ++= entities.map(entity ⇒ (id(entity), entity))
  def delete(id: Id): Unit = map.single.remove(id)
  def update(id: Id, partial: PartialEntity): Unit = throw new NotImplementedError
  def upsert(entity: Entity): UpsertStatus = throw new NotImplementedError

}

trait SalatRepository extends Repository {

  protected def mongoClient: MongoClient
  protected def dbName: String
  protected def collectionName: String
  implicit protected def context: Context
  implicit protected def idManifest: Manifest[Id]
  implicit protected def entityManifest: Manifest[Entity]
  implicit protected def partialEntityManifest: Manifest[PartialEntity]
  lazy protected val salatDao = new SalatDAO[Entity, Id](mongoClient(dbName)(collectionName)) {}

  def get(id: Id): Option[Entity] = {
    //TODO: Salat decided to disable "ById" querying on compound ids to "protect" users who don't read the docs
    //TODO: When they fix it refactor out the boilerplate from this trait
    //TODO: See https://github.com/novus/salat/issues/110 and https://github.com/novus/salat/issues/86
    //    salatDao.findOneById(id)
    salatDao.findOne(idToDBObject(id))
  }

  def getByIds(ids: Seq[Id]): Seq[Entity] = {
    if (ids.isEmpty) Seq.empty[Entity] else salatDao.find($or(ids.map("_id" -> _): _*)).toList
  }

  def getAll: Seq[Entity] =
    salatDao.find(MongoDBObject.empty).toList

  def insert(entity: Entity): Unit =
    salatDao.insert(entity)

  def insert(entities: Seq[Entity]): Unit =
    salatDao.insert(entities)

  def delete(id: Id): Unit = {
    //    salatDao.removeById(id)
    salatDao.remove(idToDBObject(id))
  }

  def update(id: Id, partialEntity: PartialEntity): Unit = {
    val dbObject = grater[PartialEntity].asDBObject(partialEntity)
    salatDao.update(idToDBObject(id), $set(dbObject.toSeq: _*), upsert = false)
  }

  def upsert(entity: Entity): UpsertStatus = {
    val writeResult = salatDao.update(idToDBObject(id(entity)), salatDao.toDBObject(entity), upsert = true)
    if (writeResult.isUpdateOfExisting) UpsertStatus.Updated else UpsertStatus.Inserted
  }

  private type CaseClass = AnyRef with Product

  def idToDBObject(id: Id): DBObject =
    if (typeOf[Id] <:< typeOf[CaseClass]) DBObject("_id" -> grater[Id].asDBObject(id)) else DBObject("_id" -> id)

}
