package com.myticket.core.repo

import com.mongodb.casbah.Imports.{ MongoClient, _ }
import com.mongodb.casbah.TypeImports._
import com.myticket.core.domain._
import com.novus.salat._

trait IMDBT extends Repository {
  type Id = String
  type Entity = IMDB
  type PartialEntity = PartialIMDB

  def save(iMDB: IMDB): WriteResult
  def id(imdb: Entity) = imdb.id
  def getByMovieName(movieName: String): Option[Entity]
  def getByCasts(data: (String, String)): List[Entity]
  def updateBoxOffice(boxOffice: Double, movieName: String): Unit
  def updateBudget(budget: Double, movieName: String): Unit
}

class IMDBRepo(val mongoClient: MongoClient, val dbName: String, val collectionName: String = "Imdb")(implicit val context: Context, val idManifest: Manifest[String], val entityManifest: Manifest[IMDB],
  val partialEntityManifest: Manifest[PartialIMDB])
    extends SalatRepository with IMDBT {

  def ensureIndex() = salatDao.collection.createIndex(DBObject("movieName" -> 1))

  override def save(iMDB: IMDB): WriteResult = {
    salatDao.update(selectByIndex(iMDB.movieName), o = salatDao._grater.asDBObject(iMDB), upsert = true, multi = false)
  }
  protected def byAttributes(data: (String, String)) = DBObject(data._1 -> data._2)

  protected def selectByIndex(movieName: String) = byAttributes("movieName", movieName)

  override def getByMovieName(movieName: String): Option[IMDB] = salatDao.findOne(byAttributes("movieName", movieName))

  override def getByCasts(data: (String, String)): List[IMDB] = salatDao.find(DBObject("casts" -> DBObject("$elemMatch" -> DBObject("$in" -> byAttributes(data), "$exists" -> true)))).toList

  override def updateBoxOffice(boxOffice: Double, movieName: String): Unit = getByMovieName(movieName).map(data ⇒ save(data.copy(boxOffice = Some(boxOffice))))

  override def updateBudget(budget: Double, movieName: String): Unit = getByMovieName(movieName).map(data ⇒ save(data.copy(budget = Some(budget))))
}

trait TheatreT extends Repository {
  type Id = String
  type Entity = Theatre
  type PartialEntity = PartialTheatre

  def save(theatre: Theatre): WriteResult
  def id(theatre: Entity) = theatre.id
  def getByName(name: String): Option[Entity]
  def addScreens(theatreId: String, screen: Set[String])
}
class TheatreRepo(val mongoClient: MongoClient, val dbName: String, val collectionName: String = "Theatre")(implicit val context: Context, val idManifest: Manifest[String], val entityManifest: Manifest[Theatre],
  val partialEntityManifest: Manifest[PartialTheatre])
    extends SalatRepository with TheatreT {

  def ensureIndex() = salatDao.collection.createIndex(DBObject("name" -> 1, "city" -> 1))

  override def save(theatre: Theatre): WriteResult = {
    salatDao.update(selectByIndex(theatre.name, theatre.city), o = salatDao._grater.asDBObject(theatre), upsert = true, multi = false)
  }
  protected def byAttributes(data: (String, String)) = DBObject(data._1 -> data._2)

  protected def selectByIndex(name: String, city: String) = byAttributes("name", name) ++ byAttributes("city", city)

  override def getByName(name: String): Option[Theatre] = salatDao.findOne(byAttributes("name", name))

  override def addScreens(theatreId: String, screen: Set[String]): Unit = get(theatreId).map {
    z ⇒
      z.copy(screenIds = z.screenIds ++ screen)
      save(z)
  }
}

trait ScreenT extends Repository {
  type Id = String
  type Entity = Screen
  type PartialEntity = PartialScreen

  def save(screen: Screen): WriteResult
  def id(screen: Entity) = screen.id
  def getByScreenType(screenType: ScreenType): List[Entity]
  def getByScreenName(screenName: String): Option[Entity]
}

class ScreenRepo(val mongoClient: MongoClient, val dbName: String, val collectionName: String = "Screen")(implicit val context: Context, val idManifest: Manifest[String], val entityManifest: Manifest[Screen],
  val partialEntityManifest: Manifest[PartialScreen])
    extends SalatRepository with ScreenT {
  def ensureIndex() = salatDao.collection.createIndex(DBObject("screenName" -> 1))

  override def save(screen: Screen): WriteResult = {
    salatDao.update(selectByIndex(screen.screenName), o = salatDao._grater.asDBObject(screen), upsert = true, multi = false)
  }

  protected def byAttributes(data: (String, String)) = DBObject(data._1 -> data._2)

  protected def selectByIndex(screenName: String) = byAttributes("screenName", screenName)

  override def getByScreenType(screenType: ScreenType): List[Screen] = getAll.filter(_.screenType == screenType).toList

  override def getByScreenName(screenName: String): Option[Screen] = salatDao.findOne(byAttributes("screenName", screenName))
}

trait ShowT extends Repository {
  type Id = String
  type Entity = Show
  type PartialEntity = PartialShow

  def save(show: Show): WriteResult
  def id(show: Entity) = show.id
  def getByIds(imdbId: String, theatreId: String): List[Entity]
  def getByMovie(imdbId: String): List[Entity]
  def getByIdsAndTime(imdbId: String, theatreId: String, screenId: String, time: Long): Option[Entity]
  def getByTime(startTime: Long, endTime: Long): List[Entity]
  def updateSeats(showId: String, seats: Set[Seat]): Unit
  def recordExcist(imdbId: String, theatreId: String, screenId: String, time: Long): Option[Show]
}

class ShowRepo(val mongoClient: MongoClient, val dbName: String, val collectionName: String = "Show")(implicit val context: Context, val idManifest: Manifest[String], val entityManifest: Manifest[Show],
  val partialEntityManifest: Manifest[PartialShow])
    extends SalatRepository with ShowT {
  def ensureIndex() = salatDao.collection.createIndex(DBObject("imdbId" -> 1, "theatreId" -> 1, "screenId" -> 1, "time" -> 1))

  override def save(show: Show): WriteResult = {
    salatDao.update(selectByIndex(show.imdbId, show.theatreId, show.screenId, show.time), o = salatDao._grater.asDBObject(show), upsert = true, multi = false)
  }

  protected def byAttributes(data: (String, String)) = DBObject(data._1 -> data._2)

  protected def selectByIndex(imdbId: String, theatreId: String, screenId: String, time: Long) = byAttributes("imdbId", imdbId) ++ byAttributes("theatreId" -> theatreId) ++ byAttributes("screenId" -> screenId) ++ DBObject("time" -> time)

  override def getByIds(imdbId: String, theatreId: String): List[Show] = salatDao.find(byAttributes("imdbId", imdbId) ++ byAttributes("theatreId", theatreId)).toList

  override def getByMovie(imdbId: String): List[Show] = salatDao.find(byAttributes("imdbId", imdbId)).toList

  override def getByIdsAndTime(imdbId: String, theatreId: String, screenId: String, time: Long): Option[Show] = salatDao.findOne(byAttributes("imdbId", imdbId) ++ byAttributes("theatreId", screenId) ++ byAttributes("screenId", screenId) ++ DBObject("time" -> time))
  override def getByTime(startIme: Long, endTime: Long): List[Show] = salatDao.find(DBObject("time" -> DBObject("$gte" -> startIme, "$lte" -> endTime))).toList

  override def updateSeats(showId: String, seats: Set[Seat]): Unit = {
    val show = get(showId)
    show.map { data ⇒
      data.availableSeatsId.toList.contains(seats) match {
        case true  ⇒ save(data.copy(availableSeatsId = data.availableSeatsId.filterNot(seats), reservedSeatsId = data.reservedSeatsId.union(seats)))
        case false ⇒
      }
    }
  }

  override def recordExcist(imdbId: String, theatreId: String, screenId: String, time: Long): Option[Show] = salatDao.findOne(selectByIndex(imdbId, theatreId, screenId, time))
}

trait OrderT extends Repository {
  type Id = String
  type Entity = Order
  type PartialEntity = PartialOrder

  def save(order: Order): WriteResult
  def id(order: Entity) = order.id
  def getByOrderStatus(orderStatus: OrderStatus): List[Entity]
  def getByTicketId(ticketId: String): Option[Entity]
  def getByUser(user: User): Option[Entity]
}

class OrderRepo(val mongoClient: MongoClient, val dbName: String, val collectionName: String = "Order")(implicit val context: Context, val idManifest: Manifest[String], val entityManifest: Manifest[Order],
  val partialEntityManifest: Manifest[PartialOrder])
    extends SalatRepository with OrderT {

  //  def ensureIndex() = salatDao.collection.createIndex(DBObject("showId" -> 1, "userId" -> 1))
  override def save(order: Order): WriteResult = {
    salatDao.update(byAttributes("_id", order.id), o = salatDao._grater.asDBObject(order), upsert = true, multi = false)
  }

  protected def byAttributes(data: (String, String)) = DBObject(data._1 -> data._2)

  protected def selectByIndex(showId: String, userId: String) = byAttributes("showId", showId) ++ byAttributes("userId" -> userId)

  override def getByOrderStatus(orderStatus: OrderStatus): List[Order] = getAll.filter(_.status == Some(orderStatus)).toList

  override def getByTicketId(ticketId: String): Option[Entity] = salatDao.findOne(byAttributes("ticketId", ticketId))

  override def getByUser(user: User): Option[Order] = salatDao.find(DBObject("userId" -> user.id, "cardId" -> user.defaultCardId.get)).sort(orderBy = DBObject("time" -> -1)).toList.filter(_.status == Some(Pending)).headOption
}