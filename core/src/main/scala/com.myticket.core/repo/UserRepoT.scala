package com.myticket.core.repo

import com.myticket.core.domain._
import com.mongodb.casbah.Imports._
import com.myticket.core.service.PasswordService
import com.novus.salat
import com.novus.salat.Context
import org.joda.time.{ DateTime, DateTimeZone }

trait UserRepoT extends Repository {
  type Entity = User
  type PartialEntity = PartialUser
  type Id = String

  def id(user: Entity) = user.id
  def getByCredentials(credentials: Credentials): Option[Entity]
  def getByName(username: String): Option[Entity]
  def updateCredentials(id: Id, credentials: Credentials): Unit
}

class MongoUserRepo(val mongoClient: MongoClient, val dbName: String, val collectionName: String = "Users")(implicit val context: Context, val idManifest: Manifest[String], val entityManifest: Manifest[User],
  val partialEntityManifest: Manifest[PartialUser])
    extends UserRepoT with SalatRepository {

  salatDao.collection.createIndex(Map("name" -> 1, "email" -> 1), Map("name" -> "name_email_index", "unique" -> true))

  def getByCredentials(credentials: Credentials): Option[Entity] = {
    val username = Map("name" -> credentials.userName)

    for {
      //First we get the user's salt
      salt ← salatDao.collection.findOne(username, fields = DBObject("salt" -> 1)).flatMap(_.getAs[String]("salt"))
      //Then we use that salt to do a query by user's username and hashed password
      //No need to enforce a transaction. If password or username changed this should just fail
      hashedPassword = PasswordService.hashedPassword(credentials.password, salt)
      user ← salatDao.findOne(username + ("hashedPassword" -> hashedPassword)) if user.status == ActiveUser
    } yield user
  }

  def getByName(username: String): Option[Entity] = salatDao.findOne(Map("name" -> username))

  def updateCredentials(id: Id, newCredentials: Credentials): Unit = {
    val salt = PasswordService.newSalt
    val hashedPassword = PasswordService.hashedPassword(newCredentials.password, salt)
    //We're losing some type safety, but I don't think this update is worth a new case class that's only used here
    val updates = $set("name" -> newCredentials.userName, "hashedPassword" -> hashedPassword, "salt" -> salt,
      "updatedOn" -> Some(DateTime.now(DateTimeZone.UTC).getMillis))

    salatDao.update(idToDBObject(id), updates, upsert = false)
  }

}

trait CardT extends Repository {
  type Id = String
  type Entity = Card
  type PartialEntity = PartialCard

  def id(card: Entity) = card.id
  def getByCardType(cardType: CardType): List[Card]
  def getByProvider(provider: String): List[Card]
  def updateCard(card: Card): Unit
  def getByBalance(id: Id, price: Double): Option[Card]
}

class MongoCardRepo(val mongoClient: MongoClient, val dbName: String, val collectionName: String = "Card")(implicit val context: Context, val idManifest: Manifest[String], val entityManifest: Manifest[Card],
  val partialEntityManifest: Manifest[PartialCard])
    extends CardT with SalatRepository {

  private def byAttributes(key: String, value: String) = salatDao.find(DBObject(key -> value)).toList

  override def getByCardType(cardType: CardType): List[Card] = getAll.filter(_.cardType == cardType).toList

  override def getByProvider(provider: String): List[Card] = byAttributes("provider", provider)

  override def updateCard(card: Card): Unit = {
    val data = salat.grater[Entity].asDBObject(card)
    salatDao.update(idToDBObject(card.id), data, false)
  }

  override def getByBalance(id: String, price: Double): Option[Card] = get(id).find(c ⇒ c.credit > price)
}

trait TransactionT extends Repository {
  type Id = String
  type Entity = Transcation
  type PartialEntity = Transcation

  def id(transaction: Entity) = transaction.id
  def getBalanceByToId(cardId: String): Double
  def getBalanceByFromId(cardId: String): Double
  def getByToId(cardId: String): List[Transcation]
  def getByFromId(cardId: String): List[Transcation]
}

class MongoTransactionRepo(val mongoClient: MongoClient, val dbName: String, val collectionName: String = "Transaction")(implicit val context: Context, val idManifest: Manifest[String], val entityManifest: Manifest[Transcation],
  val partialEntityManifest: Manifest[Transcation])
    extends TransactionT with SalatRepository {

  private def byAttributes(key: String, value: String) = salatDao.find(DBObject(key -> value)).toList

  override def getBalanceByToId(cardId: String): Double = getByToId(cardId).map(_.amount).sum

  override def getBalanceByFromId(cardId: String): Double = getByFromId(cardId).map(_.amount).sum

  override def getByToId(cardId: String): List[Transcation] = byAttributes("toId", cardId)

  override def getByFromId(cardId: String): List[Transcation] = byAttributes("fromId", cardId)
}