package com.myticket.core.repo

import java.util.UUID

import com.mongodb.DuplicateKeyException
import com.myticket.core.domain.{ Credentials, InActiveUser, PartialUser }
import com.myticket.core.domain.DomainGenerator._
import com.myticket.core.service.PasswordService
import com.novus.salat.global.ctx
import org.scalatest.{ DoNotDiscover, FlatSpecLike, MustMatchers }

@DoNotDiscover
class UserRepoTests extends FlatSpecLike with MustMatchers {

  "UserRepo" must "insert/read/delete individual User" in new RepoContext {

    val input = someUser(1).head
    repo.insert(input)
    repo.get(input.id) mustBe Some(input)
    repo.delete(input.id)
    repo.get(input.id) mustBe None
  }
  it must "get a user by their credentials" in new RepoContext {
    val salt = PasswordService.newSalt
    val fakePassword = PasswordService.hashedPassword("admin1234", salt)
    val fakeUser = someUser(1).head.copy(salt = salt, hashedPassword = fakePassword)

    repo.insert(fakeUser)

    val maybeUser = repo.getByCredentials(Credentials(fakeUser.name, "admin1234"))
    maybeUser mustBe Some(fakeUser)
  }

  it must "not get a user by their credentials if they're status is not active" in new RepoContext {
    val salt = PasswordService.newSalt
    val fakePassword = PasswordService.hashedPassword("admin1234", salt)
    val fakeUser = someUser(1).head.copy(salt = salt, hashedPassword = fakePassword)

    repo.insert(fakeUser.copy(status = InActiveUser))

    val maybeUser = repo.getByCredentials(Credentials(fakeUser.name, "admin1234"))
    maybeUser mustBe None
  }

  it must "get a user by username" in new RepoContext {
    val fakeUser = someUser(1).head
    repo.insert(fakeUser)

    val maybeUser = repo.getByName(fakeUser.name)
    maybeUser mustBe Some(fakeUser)
  }

  it must "update a user's credentials" in new RepoContext {
    val fakeUser = someUser(1).head

    repo.insert(fakeUser)

    val newCreds = Credentials(userName = "jackblack", password = "pa$$w0rd12345")
    repo.updateCredentials(fakeUser.id, newCreds)

    val updatedUser = repo.get(fakeUser.id).get
    import updatedUser._

    name mustBe newCreds.userName
    hashedPassword mustBe PasswordService.hashedPassword(newCreds.password, updatedUser.salt)
    updatedOn mustNot be(None)
    salt mustNot be(fakeUser.salt)
  }

  it must "update a user" in new RepoContext {
    val fakeUser = someUser(1).head
    repo.insert(fakeUser)

    val partial = PartialUser(email = Some("bar@foo.com"), lastName = Some("Smith"), salt = Some(fakeUser.salt))

    repo.update(fakeUser.id, partial)

    val updated = repo.get(fakeUser.id).get

    updated mustBe fakeUser.copy(email = partial.email, lastName = partial.lastName.get)
  }

  //TODO: figure out what happens with users imported from the old system that violate this rule
  it must "not allow multiple users with the same user name" in new RepoContext {
    val fakeUser = someUser(1).head
    val fakeUser2 = fakeUser

    repo.insert(fakeUser)
    //TODO: use our own custom exception so that the mongo implementation doesn't leak past the repo abstraction
    intercept[DuplicateKeyException](
      repo.insert(fakeUser2))

    val fakeUser3 = fakeUser.copy(id = "23121321", name = "john", email = Some("bar@foo.com"))
    repo.insert(fakeUser3)
  }

  trait RepoContext {
    lazy val repo: MongoUserRepo = new MongoUserRepo(EmbeddedMongoSuite.mongoClient, "testdb", UUID.randomUUID().toString)
  }
}
