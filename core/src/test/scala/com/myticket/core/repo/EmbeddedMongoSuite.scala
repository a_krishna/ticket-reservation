package com.myticket.core.repo

import com.mongodb.casbah.MongoClient
import de.flapdoodle.embed.mongo.config.{ MongodConfigBuilder, RuntimeConfigBuilder }
import de.flapdoodle.embed.mongo.distribution.Version
import de.flapdoodle.embed.mongo.{ Command, MongodStarter }
import de.flapdoodle.embed.process.config.io.ProcessOutput
import org.scalatest.{ BeforeAndAfterAll, Suites }

trait EmbeddedMongoSuite extends BeforeAndAfterAll { this: Suites ⇒
  import EmbeddedMongoSuite._

  lazy val mongodExecutable = {
    val runtimeConfig = new RuntimeConfigBuilder().defaults(Command.MongoD).processOutput(ProcessOutput.getDefaultInstanceSilent).build
    val mongodConfig = new MongodConfigBuilder().version(Version.Main.V3_0).build

    MongodStarter.getInstance(runtimeConfig).prepare(mongodConfig)
  }

  override def beforeAll(): Unit = {
    val mongodProcess = mongodExecutable.start
    mongoClient = MongoClient(mongodProcess.getConfig.net.getBindIp, mongodProcess.getConfig.net.getPort)
  }

  override def afterAll(): Unit = {
    mongoClient.close()
    mongodExecutable.stop() //This stops mongodProcess as well
  }

}

object EmbeddedMongoSuite {

  //Put this here so it can be easily accessed by all the suite's specs
  //TODO: figure out a simple way to pass this client to the tests without resorting to global, mutable state
  var mongoClient: MongoClient = _

  private[EmbeddedMongoSuite] lazy val mongodExecutable = {
    val runtimeConfig = new RuntimeConfigBuilder().defaults(Command.MongoD).processOutput(ProcessOutput.getDefaultInstanceSilent).build
    val mongodConfig = new MongodConfigBuilder().version(Version.Main.V3_0).build

    MongodStarter.getInstance(runtimeConfig).prepare(mongodConfig)
  }
}