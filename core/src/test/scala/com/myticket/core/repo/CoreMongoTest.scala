package com.myticket.core.repo

import org.scalatest.Suites

class CoreMongoTest extends Suites(new UserRepoTests, new TransactionRepoTest, new CardRepoTest, new ImdbRepoTest, new MovieTicketSystemRepoTest) with EmbeddedMongoSuite

