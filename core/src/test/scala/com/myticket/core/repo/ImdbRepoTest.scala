package com.myticket.core.repo

import java.util.UUID

import com.myticket.core.domain.DomainGenerator._
import com.novus.salat.global.ctx
import org.scalatest.{ DoNotDiscover, FlatSpecLike, MustMatchers }

@DoNotDiscover
class ImdbRepoTest extends FlatSpecLike with MustMatchers {

  "TransactionRepo" must "insert/read/delete individual Transaction" in new RepoContext {

    val input = someIMDB(1).head
    repo.insert(input)
    repo.get(input.id) mustBe Some(input)
    repo.delete(input.id)
    repo.get(input.id) mustBe None
  }
  it must "Get By Movie Name & Update  Casts" in new RepoContext {
    val input = someIMDB(1).head.copy(movieName = "Inception")
    val casts = Map("director" -> "Christoper Nolan", "producer" -> "WB", "background-score" -> "AR Rahman", "leadCastMale" -> "Leonardo DeCaprio", "leadCastFemale" -> "Jenifer Lawrence")
    repo.insert(input)
    repo.getByMovieName("Inception") mustBe Some(input)
    repo.save(input.copy(casts = casts))
    repo.get(input.id).get.casts mustBe casts
  }

  it must "Update Box Office & Update Budget" in new RepoContext {
    val input = someIMDB(1).head.copy(movieName = "Inception")
    repo.insert(input)
    repo.getByMovieName("Inception") mustBe Some(input)

    repo.updateBoxOffice(13000056, "Inception")
    repo.getByMovieName("Inception").get.boxOffice mustBe Some(13000056)

    repo.updateBudget(13000000, "Inception")
    repo.getByMovieName("Inception").get.budget mustBe Some(13000000)
  }

  trait RepoContext {
    lazy val repo: IMDBRepo = new IMDBRepo(EmbeddedMongoSuite.mongoClient, "testdb", UUID.randomUUID().toString)
  }
}

