package com.myticket.core.repo

import java.util.UUID

import com.myticket.core.domain.DomainGenerator._
import com.myticket.core.domain.{ Booked, Pending, Seat }
import com.novus.salat.global.ctx
import org.scalatest.{ DoNotDiscover, FlatSpecLike, MustMatchers }

@DoNotDiscover
class MovieTicketSystemRepoTest extends FlatSpecLike with MustMatchers {

  "TheatreRepo" must "insert/read/delete individual Theatre" in new RepoContext {

    val input = someTheatre(1).head
    repoT.insert(input)
    repoT.get(input.id) mustBe Some(input)
    repoT.delete(input.id)
    repoT.get(input.id) mustBe None
  }

  it must "ScreenRepo insert/read/delete individual Screen" in new RepoContext {

    val input = someScreen(1).head
    repoScr.insert(input)
    repoScr.get(input.id) mustBe Some(input)
    repoScr.delete(input.id)
    repoScr.get(input.id) mustBe None
  }

  it must "ShowRepo insert/read/delete individual Show" in new RepoContext {

    val input = someShow(1).head
    repoSh.insert(input)
    repoSh.get(input.id) mustBe Some(input)
    repoSh.delete(input.id)
    repoSh.get(input.id) mustBe None
  }

  it must "OrderRepo insert/read/delete individual Order" in new RepoContext {

    val input = someOrder(1).head
    repoOrder.insert(input)
    repoOrder.get(input.id) mustBe Some(input)
    repoOrder.delete(input.id)
    repoOrder.get(input.id) mustBe None
  }

  it must "Create Screen Based on Theatre " in new RepoContext {

    val screen = someScreen(1).head
    val theatre = someTheatre(1).head.copy(screenIds = Set(screen.id), name = "PVR")

    repoScr.insert(screen)
    repoT.insert(theatre)

    val resutlTheatre = repoT.getByName("PVR")
    resutlTheatre.get.screenIds mustBe theatre.screenIds

    val resultScreen = repoScr.getByIds(resutlTheatre.get.screenIds.toList)
    resultScreen mustBe Seq(screen)

  }

  it must "Create Show Based place Order " in new RepoContext {
    val seats = someSeat(5) ++ Set(Seat("A1", "GOLD", 200.0D))
    val screen = someScreen(1).head.copy(seatsId = seats.toSet)
    val theatre = someTheatre(1).head.copy(screenIds = Set(screen.id), name = "PVR")
    val show = someShow(1).head.copy(theatreId = theatre.id, screenId = screen.id, availableSeatsId = seats.toSet)
    val order = someOrder(1).head.copy(showId = show.id, status = Some(Pending), seats = Set(Seat("A1", "GOLD", 200.0D)))

    repoScr.save(screen)
    repoT.save(theatre)
    repoSh.save(show)
    repoOrder.save(order)

    val resutlTheatre = repoT.getByName("PVR")
    resutlTheatre.get.screenIds mustBe theatre.screenIds

    val resultScreen = repoScr.getByIds(resutlTheatre.get.screenIds.toList)
    resultScreen mustBe Seq(screen)

    repoOrder.getByOrderStatus(Pending) mustBe List(order)

    repoSh.updateSeats(show.id, order.seats)

    repoOrder.upsert(order.copy(status = Some(Booked), ticketId = Some("xyz")))

    repoOrder.getByTicketId("xyz").get.status mustBe Some(Booked)
  }

  trait RepoContext {
    lazy val repoT: TheatreRepo = new TheatreRepo(EmbeddedMongoSuite.mongoClient, "testdb1", UUID.randomUUID().toString)
    lazy val repoScr: ScreenRepo = new ScreenRepo(EmbeddedMongoSuite.mongoClient, "testdb4", UUID.randomUUID().toString)
    lazy val repoSh: ShowRepo = new ShowRepo(EmbeddedMongoSuite.mongoClient, "testdb5", UUID.randomUUID().toString)
    lazy val repoOrder: OrderRepo = new OrderRepo(EmbeddedMongoSuite.mongoClient, "testdb6", UUID.randomUUID().toString)

    val sClass = Seq(("class1", "SLIVER", 100.0D), ("class2", "GOLD", 150.0D), ("class3", "PLATINUM", 300.0D))
    val row = 'a' to 'c' map (_.toString)
    def seat(data: IndexedSeq[(String, String)]): List[(String, String, String)] = data.flatMap(z ⇒ (1 to 5).map(x ⇒ (z._1, x + z._2))).foldLeft(List.empty[(String, String, String)])((z, a) ⇒ z ++ List((s"seat${z.size + 1}", a._2, a._1)))
  }
}

