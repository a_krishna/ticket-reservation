package com.myticket.core.repo

import java.util.UUID

import com.myticket.core.domain.{ Credit, Debit, PartialCard, Wallet }
import org.scalatest.{ DoNotDiscover, FlatSpecLike, MustMatchers }
import com.myticket.core.domain.DomainGenerator._
import com.novus.salat.global.ctx

@DoNotDiscover
class CardRepoTest extends FlatSpecLike with MustMatchers {

  "CardRepo" must "insert/read/delete individual Card" in new RepoContext {

    val input = somecard(1).head
    repo.insert(input)
    repo.get(input.id) mustBe Some(input)
    repo.delete(input.id)
    repo.get(input.id) mustBe None
  }

  it must "update & get by card type" in new RepoContext {
    val input = somecard(1).head.copy(cardType = Debit)
    val input1 = somecard(10) ++ Seq(input)

    repo.insert(input1)

    repo.getByCardType(Wallet).size mustBe 10

    val card = repo.getByCardType(Debit)

    card.size mustBe 1

    repo.updateCard(card.head.copy(cardType = Credit))

    repo.getByCardType(Debit).size mustBe 0
  }

  it must "get by Card & Provider" in new RepoContext {
    val input = somecard(20).map(_.copy(cardType = Debit, provider = Some("CITI")))
    val input1 = somecard(10)

    repo.insert(input ++ input1)

    repo.getByCardType(Debit).size mustBe 20

    repo.getByProvider("CITI").size mustBe 20

  }

  trait RepoContext {
    lazy val repo: MongoCardRepo = new MongoCardRepo(EmbeddedMongoSuite.mongoClient, "testdb", UUID.randomUUID().toString)
  }
}
