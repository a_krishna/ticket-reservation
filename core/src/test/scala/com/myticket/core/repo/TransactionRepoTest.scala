package com.myticket.core.repo

import java.util.UUID

import org.scalatest.{ DoNotDiscover, FlatSpecLike, MustMatchers }
import com.myticket.core.domain.DomainGenerator._
import com.novus.salat.global.ctx

@DoNotDiscover
class TransactionRepoTest extends FlatSpecLike with MustMatchers {

  "TransactionRepo" must "insert/read/delete individual Transaction" in new RepoContext {

    val input = someTransaction(1).head
    repo.insert(input)
    repo.get(input.id) mustBe Some(input)
    repo.delete(input.id)
    repo.get(input.id) mustBe None
  }

  it must "Bulk Insert positive balance" in new RepoContext {
    val card1 = "card1"
    val card2 = "card2"
    val input1 = someTransaction(10).map(_.copy(fromId = card1, toId = card2, amount = 299.0))
    val input2 = someTransaction(10).map(_.copy(fromId = card2, toId = card1, amount = 99.0))
    repo.insert(input1 ++ input2)
    repo.getBalanceByFromId(card1) mustBe (2990.0)
    repo.getBalanceByToId(card1) mustBe (990.0)
  }

  it must "Bulk Insert negative balance" in new RepoContext {
    val card1 = "card1"
    val card2 = "card2"
    val input1 = someTransaction(10).map(_.copy(fromId = card1, toId = card2, amount = 299.0))
    val input2 = someTransaction(10).map(_.copy(fromId = card2, toId = card1, amount = 99.0))
    val input3 = someTransaction(10).map(_.copy(fromId = card1, toId = card2, amount = -399.0))
    repo.insert(input1 ++ input2 ++ input3)
    repo.getBalanceByFromId(card1) mustBe (-1000.0)
  }

  trait RepoContext {
    lazy val repo: MongoTransactionRepo = new MongoTransactionRepo(EmbeddedMongoSuite.mongoClient, "testdb", UUID.randomUUID().toString)
  }
}

