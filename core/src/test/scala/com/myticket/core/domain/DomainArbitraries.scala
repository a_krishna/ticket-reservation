package com.myticket.core.domain

import org.joda.time.{ DateTime, DateTimeZone }
import org.scalacheck.Arbitrary
import org.scalacheck.Arbitrary._
import org.scalacheck.Gen._
import scalaCheck._

trait DomainArbitraries {

  implicit val transcation: Arbitrary[Transcation] = Arbitrary[Transcation] {
    for {
      id ← nonEmptyAlphaStr
      fromId ← nonEmptyAlphaStr
      toId ← nonEmptyAlphaStr
      amount ← posNum[Double]
      createdOn ← const(DateTime.now(DateTimeZone.UTC).getMillis)
    } yield Transcation(id, fromId, toId, amount, createdOn)
  }

  implicit val card: Arbitrary[Card] = Arbitrary[Card] {
    for {
      id ← nonEmptyAlphaStr
      name ← nonEmptyAlphaStr
      cardType ← const(Wallet)
      credit ← posNum[Double]
      provider ← option(nonEmptyAlphaStr)
      expiryDate ← option((DateTime.now(DateTimeZone.UTC).plusYears(12).withTimeAtStartOfDay().getMillis))
      createdOn ← const(DateTime.now(DateTimeZone.UTC).getMillis)
    } yield Card(id, name, cardType, credit, provider, expiryDate, createdOn, None)
  }

  implicit val user: Arbitrary[User] = Arbitrary[User] {
    for {
      id ← nonEmptyAlphaStr
      name ← nonEmptyAlphaStr
      password ← nonEmptyAlphaStr
      email ← option(nonEmptyAlphaStr)
      role ← const(NormalUser)
      salt ← nonEmptyAlphaStr
      fName ← nonEmptyAlphaStr
      lName ← nonEmptyAlphaStr
      cardId ← option(nonEmptyAlphaStr)
      createdOn ← const(DateTime.now(DateTimeZone.UTC).getMillis)
      status ← const(ActiveUser)
    } yield User(id, name, password, email, role, salt, fName, lName, List.empty[String], List.empty[String], cardId, List.empty[String], createdOn, None, status)
  }

  implicit val imdb: Arbitrary[IMDB] = Arbitrary[IMDB] {
    for {
      id ← nonEmptyAlphaStr
      movieName ← nonEmptyAlphaStr
      releaseDate ← option(DateTime.now(DateTimeZone.UTC).plusDays(1).getMillis)
      budget ← option(posNum[Double])
      boxOffice ← option(posNum[Double])
      rating ← option(posNum[Long])
      createdOn ← const(DateTime.now(DateTimeZone.UTC).getMillis)
    } yield IMDB(id, movieName, releaseDate, budget, boxOffice, rating, List.empty, createdOn, None, Map.empty)
  }

  implicit val theatre: Arbitrary[Theatre] = Arbitrary[Theatre] {
    for {
      id ← nonEmptyAlphaStr
      name ← nonEmptyAlphaStr
      city ← nonEmptyAlphaStr
      state ← nonEmptyAlphaStr
      country ← nonEmptyAlphaStr
      screenId ← listOfN(10, nonEmptyAlphaStr).map(_.toSet)
      rating ← option(posNum[Long])
      createdOn ← const(DateTime.now(DateTimeZone.UTC).getMillis)
    } yield Theatre(id, name, city, state, country, screenId, rating, List.empty, createdOn, None)
  }

  implicit val seat: Arbitrary[Seat] = Arbitrary[Seat] {
    for {
      name ← nonEmptyAlphaStr
      classId ← nonEmptyAlphaStr
      price ← const(100.0D)
    } yield Seat(name, classId, price)
  }

  implicit val screen: Arbitrary[Screen] = Arbitrary[Screen] {
    for {
      id ← nonEmptyAlphaStr
      name ← nonEmptyAlphaStr
      _type ← `3D`
      priceAddOn ← `3D`.price
      capacity ← option(100L)
      createdOn ← const(DateTime.now(DateTimeZone.UTC).getMillis)
    } yield Screen(id, name, _type, Set.empty[Seat], capacity, priceAddOn, None, createdOn, None)
  }

  implicit val show: Arbitrary[Show] = Arbitrary[Show] {
    for {
      id ← nonEmptyAlphaStr
      imdbId ← nonEmptyAlphaStr
      theatreId ← nonEmptyAlphaStr
      screenId ← nonEmptyAlphaStr
      time ← const(DateTime.now(DateTimeZone.UTC).plusDays(1).getMillis)
      createdOn ← const(DateTime.now(DateTimeZone.UTC).getMillis)
    } yield Show(id, imdbId, theatreId, screenId, time, Set.empty[Seat], Set.empty[Seat], createdOn, None)
  }

  implicit val order: Arbitrary[Order] = Arbitrary[Order] {
    for {
      id ← nonEmptyAlphaStr
      show ← nonEmptyAlphaStr
      user ← nonEmptyAlphaStr
      card ← nonEmptyAlphaStr
      price ← posNum[Double]
      createdOn ← const(DateTime.now(DateTimeZone.UTC).getMillis)
    } yield Order(id, show, user, Set.empty[Seat], card, price, None, None, createdOn, None)
  }
}

object DomainArbitraries extends DomainArbitraries