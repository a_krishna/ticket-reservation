package com.myticket.core.domain

import org.scalacheck.Gen

object scalaCheck {

  /** Creates a stream of samples from the specified generator. */
  def samples[A](g: Gen[A]): Stream[Option[A]] =
    Stream.continually(g.sample)

  /** Provides a small vector of samples from the specified generator. */
  def previewGen[A](g: Gen[A], size: Int = 20): Vector[Option[A]] =
    samples(g).take(size).toVector

  /**
   * Generator of alpha strings that have at least one character, and are about the size you might use
   * for a key. The intent is to minimize the space taken in debugging output.
   */
  def shortAlphaString: Gen[String] = alphaStr(min = 1, max = 8)

  /** Generator of alpha strings that have at least one character. */
  def nonEmptyAlphaStr: Gen[String] = alphaStr(min = 1, max = 100)

  /** Generator of alpha strings that of random length between the specified min and max. */
  def alphaStr(min: Int, max: Int): Gen[String] = {
    listOfRandomN(Gen.alphaChar, min, max) map {
      _.mkString
    }
  }

  /** Generator of Option[String] that properly returns None in case of empty String **/
  def alphaStrOption: Gen[Option[String]] = Gen.alphaStr map { s ⇒ if (s.nonEmpty) Some(s) else None }

  /** Generator of random sized list [min, max] items. */
  def listOfRandomN[A](g: Gen[A], min: Int = 0, max: Int = 25): Gen[List[A]] =
    Gen.choose(min, max) flatMap { size ⇒ Gen.listOfN(size, g) }

  /** Generator of an `Option[A]` given a `Gen[A]`. */
  def genOption[A](g: Gen[A]): Gen[Option[A]] =
    listOfRandomN(g, 0, 1) map {
      _.headOption
    }
}

