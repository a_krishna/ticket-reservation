package com.myticket.core.domain

import DomainArbitraries._
import scalaCheck._
import org.scalacheck.Arbitrary._

object DomainGenerator {

  def someTransaction(size: Int = 10) = previewGen(arbitrary[Transcation], size).flatten.toIndexedSeq
  def somecard(size: Int = 10) = previewGen(arbitrary[Card], size).flatten.toIndexedSeq
  def someUser(size: Int = 10) = previewGen(arbitrary[User], size).flatten.toIndexedSeq
  def someIMDB(size: Int = 10) = previewGen(arbitrary[IMDB], size).flatten.toIndexedSeq
  def someTheatre(size: Int = 10) = previewGen(arbitrary[Theatre], size).flatten.toIndexedSeq
  def someSeat(size: Int = 10) = previewGen(arbitrary[Seat], size).flatten.toIndexedSeq
  def someScreen(size: Int = 10) = previewGen(arbitrary[Screen], size).flatten.toIndexedSeq
  def someShow(size: Int = 10) = previewGen(arbitrary[Show], size).flatten.toIndexedSeq
  def someOrder(size: Int = 10) = previewGen(arbitrary[Order], size).flatten.toIndexedSeq
}
