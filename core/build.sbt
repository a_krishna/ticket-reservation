
ivyScala := ivyScala.value map { _.copy(overrideScalaVersion = true) }

libraryDependencies ++= Seq(
  "ch.qos.logback"              % "logback-classic"                        % Versions.Logback              % Compile,
  "ch.qos.logback"              % "logback-core"                           % Versions.Logback              % Compile,
  "org.mongodb"                %% "casbah"                                 % Versions.Casbah               % Compile,
  "com.typesafe.play"          %% "play-json"                              % Versions.PlayJson             % Compile,
  "com.novus"                  %% "salat"                                  % Versions.Salat                % Compile,
  "org.scalaz"                 %% "scalaz-core"                            % Versions.Scalaz                % Compile,
  "com.typesafe.akka"          %% "akka-camel"                             % Versions.Akka                 % Compile,
  "org.scalatest"              %% "scalatest"                              % Versions.ScalaTest            % "test",
  "de.flapdoodle.embed"         % "de.flapdoodle.embed.mongo"              % Versions.EmbedMongo           % "test",
  "net.logstash.logback"        % "logstash-logback-encoder"               % Versions.LogstashEncoder      % Runtime,
  "org.scalacheck"             %% "scalacheck"                             % Versions.ScalaCheck           % Optional
)
