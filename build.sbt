name := "ticket-reservation-system"

organization in ThisBuild := "com.myticket"

lazy val core = Project("myticket-core", file("core"))

lazy val service = Project("myticket-service", file("service")).configs(IntegrationTest).settings(Defaults.itSettings: _*).dependsOn(core %"compile->compile;test->test",core)

scalaVersion in ThisBuild := "2.11.7"

conflictWarning in ThisBuild := ConflictWarning.disable

parallelExecution in Test in ThisBuild := false

parallelExecution in IntegrationTest in ThisBuild := false 

scalacOptions in Compile in ThisBuild ++= Seq("-unchecked", "-deprecation", "-encoding", "utf8", "-language:postfixOps")

logLevel in compile := Level.Error
