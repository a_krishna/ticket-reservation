name := "myticket-service"

ivyScala := ivyScala.value map { _.copy(overrideScalaVersion = true) }

libraryDependencies ++= Seq(
  "com.typesafe.play"          %% "play-json"                              % Versions.PlayJson             % Compile,
  "com.typesafe.akka"          %% "akka-actor"                             % Versions.Akka                 % Compile,
  "com.typesafe.akka"          %% "akka-cluster"                           % Versions.Akka                 % Compile,
  "com.typesafe.akka"          %% "akka-remote"                            % Versions.Akka                 % Compile
    exclude ("io.netty", "netty"),
  "com.typesafe.akka"          %% "akka-slf4j"                             % Versions.Akka                 % Compile,
  "com.typesafe.scala-logging" %% "scala-logging"                          % Versions.ScalaLogging         % Compile,
  "ch.qos.logback"             % "logback-classic"                         % Versions.Logback              % Runtime,
  "io.spray"                   %% "spray-can"                              % Versions.Spray                % Compile,
  "io.spray"                   %% "spray-http"                             % Versions.Spray                % Compile,
  "io.spray"                   %% "spray-routing"                          % Versions.Spray                % Compile,
  "io.spray"                   %% "spray-httpx"                            % Versions.Spray                % Compile,
  "io.spray"                   %% "spray-client"                           % Versions.Spray                % Compile,
  "io.spray"                   %% "spray-caching"                          % Versions.Spray                % Compile,
  "com.novus"                  %% "salat"                                  % Versions.Salat                % Compile,
  "org.scalaz"                 %% "scalaz-core"                            % Versions.Scalaz               % Compile,
  "org.scalaz"                 %% "scalaz-concurrent"                      % Versions.Scalaz               % Compile,
  "org.scalaz"                 %% "scalaz-effect"                          % Versions.Scalaz               % Compile,
  "org.scalatest"              %% "scalatest"                              % Versions.ScalaTest            % Test,
  "org.scalacheck"             %% "scalacheck"                             % Versions.ScalaCheck           % Optional,
  "org.slf4j"                   % "log4j-over-slf4j"                       % Versions.Log4jOverSlf4j       % Runtime,
  "com.typesafe.akka"          %% "akka-testkit"                           % Versions.Akka                 % Test,
  "io.spray"                   %% "spray-testkit"                          % Versions.Spray                % Test,
  "org.scalamock"              %% "scalamock-scalatest-support"            % Versions.ScalaMock            % "it, test",
  "org.mockito"                  % "mockito-core"                           % "1.10.19"                    % Test,
  "net.logstash.logback"        % "logstash-logback-encoder"               % Versions.LogstashEncoder      % Runtime,
  "com.miguno.akka"             %% "akka-mock-scheduler"            % "0.4.0"         % Test
)

enablePlugins(BuildInfoPlugin)
buildInfoOptions += BuildInfoOption.ToMap
buildInfoPackage := "com.myticket.service"
//Assemby settings
test in assembly := {}
assemblyShadeRules in assembly := Seq(
  ShadeRule.rename("com.typesafe.scalalogging.**" -> "shade.com.typesafe.scalalogging.@1")
    .inLibrary("com.typesafe.scala-logging" % "scala-logging-api_2.11" % "2.1.2")
    .inLibrary("com.typesafe.scala-logging" % "scala-logging-slf4_2.11" % "2.1.2")
)

//Make assembly a publishable artifact
artifact in (Compile, assembly) := {
  val art = (artifact in (Compile, assembly)).value
  art.copy(`classifier` = Some("assembly"))
}

addArtifact(artifact in (Compile, assembly), assembly)
