package com.myticket.service.actors

import akka.actor.SupervisorStrategy.{ Restart, Resume }
import akka.actor.{ Actor, ActorLogging, OneForOneStrategy, PoisonPill, Props, StopChild, Terminated }
import com.myticket.core.domain._
import com.myticket.core.repo.OrderRepo
import com.myticket.service.api.TicketService
import com.myticket.service.domain.Ticket

import scala.concurrent.duration._
import scala.util.{ Failure, Success }

case object BookingStatus

class TicketWatcherManager(ticketService: TicketService) extends Actor with ActorLogging {

  override val supervisorStrategy = OneForOneStrategy(maxNrOfRetries = 3) {
    case _: Exception ⇒ Resume
  }

  override def preStart(): Unit = {
    context.system.eventStream.subscribe(self, classOf[Ticket])
  }

  override def postStop(): Unit = {
    context.system.eventStream.unsubscribe(self, classOf[Ticket])
  }

  def getChildName(t: Ticket) = s"${t.movieName.getOrElse("")}-${t.screenName.getOrElse("")}-${t.showTime.getOrElse(0L)}-${t.userId.getOrElse("")}"
  def receive = {
    case ts: Ticket ⇒ ts.status match {
      case Pending ⇒ context.child(getChildName(ts)) match {
        case Some(c) ⇒
        case None    ⇒ context.actorOf(Props(new TicketWatcher(ts.ticketId, ticketService)), getChildName(ts))
      }
      case _ ⇒
    }
    case m @ Terminated(child) ⇒ context.unwatch(child)
  }
}

class TicketWatcher(ticketId: String, ticketService: TicketService) extends Actor with ActorLogging {

  import context.dispatcher

  override def preStart(): Unit = {
    context.system.eventStream.subscribe(self, classOf[Ticket])
    context.system.scheduler.schedule(15.minutes, 0.minutes, self, BookingStatus)
  }

  override def postStop(): Unit = {
    context.system.eventStream.unsubscribe(self, classOf[Ticket])
  }

  def receive = {
    case ts: Ticket ⇒ ts.status match {
      case Booked       ⇒ self ! PoisonPill
      case Pending      ⇒
      case Rejected     ⇒ self ! PoisonPill
      case NotAvailable ⇒ self ! PoisonPill
      case Cancel       ⇒ self ! PoisonPill
    }
    case BookingStatus ⇒ ticketService.verifyReturn(ticketId) onComplete {
      case Success(data) ⇒
        log.info(s" Retain Status ${data}")
        self ! PoisonPill
      case Failure(e) ⇒
        log.error(s"${e.getMessage}")
        self ! PoisonPill
    }
    case _ ⇒
  }
}

