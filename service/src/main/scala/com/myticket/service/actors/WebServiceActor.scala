package com.myticket.service.actors

import java.util.UUID

import akka.actor.{ ActorLogging, ActorRef, Props }
import akka.io.IO
import com.mongodb.DuplicateKeyException
import com.myticket.core.ObjectId
import com.myticket.core.domain.{ Admin, Card, NormalUser, User }
import com.myticket.core.repo.MongoUserRepo
import com.myticket.core.service.{ PasswordService, ValidationError }
import com.myticket.service.Settings
import com.myticket.service.api.{ SessionService, TheatreService, TicketService }
import com.myticket.service.routes._
import spray.can.Http
import spray.http.StatusCodes
import spray.httpx.PlayJsonSupport
import spray.routing.Directives.complete
import spray.routing.{ ExceptionHandler, HttpServiceActor }

import scala.collection.immutable.Seq

class WebServiceActor(host: String, port: Int,
  userRepo: MongoUserRepo, ticketService: TicketService,
  theatreService: TheatreService, sessionCacheActor: ActorRef) extends HttpServiceActor
    with ActorLogging
    with CORSDirectives
    with UsersRoute
    with SessionsRoute
    with ShowRoutes
    with TicketRoutes
    with ScreenRoute {

  import WebServiceActor.exceptionHandler
  import context.dispatcher //Used mainly by routes that need thread pools for Futures/asks

  //Start this actor as a web server
  IO(Http)(context.system) ! Http.Bind(listener = self, interface = host, port = port)

  implicit val webServiceTimeout = Settings.webServiceTimeout

  implicit val system = context.system

  val sessionAuthenticator = SessionService.fakeSessionAuthenticator(WebServiceActor.groupUser)
  //  val sessionAuthenticator = SessionService.sessionAuthenticator(system)

  //Aggregate and run all the routes
  def receive = runRoute(
    cors(EchoAllOriginsAccepted)(
      usersRoute(sessionAuthenticator, userRepo, sessionCacheActor) ~
        sessionsRoute(sessionAuthenticator, userRepo, sessionCacheActor) ~
        ticketRoute(sessionAuthenticator, ticketService) ~
        screenRoute(sessionAuthenticator, theatreService) ~
        showRoute(sessionAuthenticator, ticketService)))

}
object WebServiceActor extends PlayJsonSupport {

  //We MUST explicitly declare the type, ExceptionHandler, or it doesn't get properly, implicitly passed to runRoute
  implicit val exceptionHandler: ExceptionHandler = ExceptionHandler {
    case e: DuplicateKeyException ⇒ complete((StatusCodes.BadRequest, Seq(ValidationError.DuplicateKey))) //TODO: move this handler to the routes it applies to
  }

  val saltUserAdmin = PasswordService.newSalt
  val saltUser = PasswordService.newSalt
  val card = Card(name = "AdminAccount", credit = 200000)
  val userAdmin = User(name = "MyTicketAdmin", role = Admin, firstName = "Myticket", lastName = "AdminRole", email = Some("myticketUser@new.com"), salt = saltUserAdmin, hashedPassword = PasswordService.hashedPassword("1234567as", saltUserAdmin), defaultCardId = Some(card.id))
  val userRole = User(name = "TicketUser", role = NormalUser, firstName = "Ticket", lastName = "UserA", email = Some("ticket@new.com"), salt = saltUser, hashedPassword = PasswordService.hashedPassword("12345678", saltUser), defaultCardId = Some(card.copy(id = ObjectId(), name = "user1").id))

  val groupUser = Map(UUID.randomUUID() -> userAdmin, UUID.randomUUID() -> userRole)
}