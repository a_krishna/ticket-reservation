package com.myticket.service.actors

import java.util.UUID

import akka.actor.{ Actor, ActorLogging, Cancellable, Scheduler }
import com.myticket.core.domain.{ ActiveUser, User }
import com.myticket.core.repo.MongoUserRepo
import com.myticket.service.domain._
import org.joda.time.DateTime

import scala.concurrent.duration.{ Duration, FiniteDuration }

class SessionActor(user: User, userRepo: MongoUserRepo, expiration: Duration, scheduler: Scheduler, metrics: Boolean) extends Actor with ActorLogging {

  var session = {
    val now = DateTime.now()
    Session(id = UUID.fromString(self.path.name), createdOn = now, touchedOn = now, user = user)
  }
  var scheduledExpiration = newScheduledExpiration

  def receive = {
    case GetSession(_) ⇒
      scheduledExpiration.cancel()

      sender() ! Some(session)
      session = session.copy(touchedOn = DateTime.now())

      scheduledExpiration = newScheduledExpiration

    case RefreshSessions(userId) if userId == session.user.id ⇒
      userRepo.get(userId) match {
        case Some(refreshedUser) if refreshedUser.status == ActiveUser ⇒
          scheduledExpiration.cancel()

          session = session.copy(touchedOn = DateTime.now(), user = refreshedUser)

          scheduledExpiration = newScheduledExpiration

        case _ ⇒
          context.stop(self)
      }
  }

  override def postStop() = scheduledExpiration.cancel()

  def newScheduledExpiration = expiration match {
    case duration: FiniteDuration ⇒ scheduler.scheduleOnce(duration) {
      log.info("User session '{}' expired at time '{}'", session.id, DateTime.now())

      context.stop(self)
    }(context.dispatcher)

    case _ ⇒
      SessionActor.noopCancellable //Infinite duration means session will never expire. So don't schedule anything
  }

}

object SessionActor {

  private val noopCancellable: Cancellable = new Cancellable {
    override val isCancelled = false
    override val cancel = false
  }

}
