package com.myticket.service.actors

import akka.actor.ActorSystem

trait EventService[Event <: AnyRef] {
  def publish[T <: Event](event: T): Unit
}

class AkkaEventService[Event <: AnyRef](implicit system: ActorSystem) extends EventService[Event] {
  override def publish[T <: Event](event: T) = system.eventStream.publish(event)
}