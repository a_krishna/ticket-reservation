package com.myticket.service.actors

import akka.actor.{ Actor, ActorLogging, OneForOneStrategy, Props, SupervisorStrategy }
import com.myticket.core.repo.MongoUserRepo
import com.myticket.service.Settings
import com.myticket.service.domain._
import org.joda.time.DateTime

import scala.util.control.NonFatal

/**
 * Singleton actor in charge of coordinating access to sessions
 * Each session is a [[SessionActor]] and a child of this actor
 *
 * @param userRepo Repository passed to each session, allowing the session to refresh itself by retrieving its updated user
 */
class SessionCacheActor(userRepo: MongoUserRepo, metrics: Boolean) extends Actor with ActorLogging {

  def receive = {
    case message @ GetSession(sessionId) ⇒
      //If the specified session id has an associated session/actor, forward this request to it
      //Session request responses are Option[Session]
      context.child(sessionId.toString) match {
        case Some(sessionActor) ⇒ sessionActor forward message
        case None               ⇒ sender() ! None
      }

      if (sessionId != Settings.apiSessionId) {
        log.debug(s"Getting session '$sessionId'")
      }

    case message: RefreshSessions ⇒
      //Sessions are refreshed by user id. We can only look up a session by its id, not by its user's id. So we have to
      //just publish this message publicly. The appropriate session(s) should handle it - the rest will ignore
      context.system.eventStream.publish(message)

      log.debug("Refreshing sessions for user id '{}'", message.userId)

    case CreateSession(sessionId, user, expiration) ⇒
      val sessionActor = context.actorOf(Props(new SessionActor(user, userRepo, expiration, context.system.scheduler, metrics)),
        name = sessionId.toString)
      context.system.eventStream.subscribe(sessionActor, classOf[RefreshSessions])

      if (sessionId != Settings.apiSessionId) {
        log.info("User '{}' in organization '{}' logged in with session id '{}' at time '{}'", user.name, sessionId, DateTime.now())
      }

    case DeleteSession(sessionId) if sessionId != Settings.apiSessionId ⇒
      context.child(sessionId.toString).foreach(context.stop)

      log.info("User with session id '{}' logged out at time '{}'", sessionId, DateTime.now())
  }

  override val supervisorStrategy =
    OneForOneStrategy() {
      case NonFatal(e) ⇒
        log.error(e, "Session crashed")
        SupervisorStrategy.Stop //Sessions are ephemeral. If there's any trouble just kill it
    }

}
