package com.myticket.service.domain

import java.util.UUID

import com.myticket.core.domain.User
import org.bson.types.ObjectId
import org.joda.time.DateTime
import play.api.libs.json.Json

import scala.concurrent.duration.Duration

case class Session(id: UUID, createdOn: DateTime, touchedOn: DateTime, user: User)

object Session {

  implicit val formatter = Json.format[Session]

}

sealed trait SessionCacheMessage
case class GetSession(sessionId: UUID) extends SessionCacheMessage
case class CreateSession(sessionId: UUID, user: User, expiration: Duration) extends SessionCacheMessage
case class DeleteSession(sessionId: UUID) extends SessionCacheMessage
case class RefreshSessions(userId: String) extends SessionCacheMessage