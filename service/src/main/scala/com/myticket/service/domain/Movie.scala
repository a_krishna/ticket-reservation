package com.myticket.service.domain

import com.myticket.core.domain.{ Credentials, OrderStatus, ScreenType, Seat }
import com.myticket.core.service.ValidationError
import com.myticket.core.service.ValidationService._
import com.myticket.service.domain.PartialCreateShow.validationErrors
import play.api.libs.json._

case class SeatSpecification(seatClass: String, price: Double, row: Int, column: Int)

case class TheatreScreen(screenName: String, screenType: ScreenType, seats: List[SeatSpecification])

object TheatreScreen {
  implicit val formatSeat = Format(Json.reads[SeatSpecification], Json.writes[SeatSpecification])
  implicit val format = Format(Json.reads[TheatreScreen], Json.writes[TheatreScreen])
}

sealed trait ShowStatus
case object ShowCreated extends ShowStatus
case object ShowExists extends ShowStatus
case object ShowCancelled extends ShowStatus

case class CreateShow(imdbId: String, theatreId: String, screenId: String, time: Long, row: Int, column: Int)

object CreateShow {
  implicit val createShow = Format(Json.reads[CreateShow], Json.writes[CreateShow])
  implicit val validator: Validator[CreateShow] = createShow ⇒ {
    import createShow._
    createValidated(success = createShow, errors = validationErrors(Some(imdbId), Some(theatreId), Some(screenId), Some(time), Some(row), Some(column)))
  }
}

case class PartialCreateShow(imdbId: Option[String], theatreId: Option[String], screenId: Option[String], time: Option[Long], row: Option[Int], column: Option[Int])

object PartialCreateShow {
  implicit val validator: Validator[PartialCreateShow] = partialCreateShow ⇒ {
    import partialCreateShow._
    createValidated(success = partialCreateShow, errors = validationErrors(imdbId, theatreId, screenId, time, row, column))
  }
  def validationErrors(imdbId: Option[String], theatreId: Option[String], screenId: Option[String], time: Option[Long], row: Option[Int], column: Option[Int]) = {
    import scalaz.std.option._
    import scalaz.syntax.traverse._

    coalesceValidationErrors(
      imdbId.traverse[Validated, String](checkNonEmptyString(_, ValidationError("MovieRequired"))),
      theatreId.traverse[Validated, String](checkNonEmptyString(_, ValidationError("TheatreRequired"))),
      screenId.traverse[Validated, String](checkNonEmptyString(_, ValidationError("ScreenRequired"))),
      time.traverse[Validated, Long](checkTimingLong(_, ValidationError("Show Creation should advance"))),
      row.traverse[Validated, Int](checkPositiveNumber(_, ValidationError("Need Positive Number Select Row"))),
      column.traverse[Validated, Int](checkPositiveNumber(_, ValidationError("Need Positive Number Select Column"))))
  }
}
sealed trait TicketStatus
case object TicketPublished extends TicketStatus
case object TicketCancelled extends TicketStatus
case object TicketBooked extends TicketStatus
case object ParkedTicket extends TicketStatus

sealed trait BookingEvent

case class Ticket(ticketId: String, movieName: Option[String] = None, showTime: Option[Long] = None, screenName: Option[String] = None, seats: Set[String] = Set.empty[String], price: Double, userId: Option[String] = None, status: OrderStatus) extends BookingEvent

object Ticket {
  import OrderStatus._
  implicit val ticket = Format(Json.reads[Ticket], Json.writes[Ticket])
}
case class TheatreView(name: String, city: String, state: String, country: String)

object TheatreView {
  implicit val theaterView = Format(Json.reads[TheatreView], Json.writes[TheatreView])
}

case class ShowView(showId: String, movieName: String, theatreView: Option[TheatreView] = None, screenName: Option[String] = None, timing: Long, availableSeats: List[Seat], reservedSeats: List[Seat])

object ShowView {
  implicit val showView = Format(Json.reads[ShowView], Json.writes[ShowView])
}

case class BookShow(showId: String, seats: Seq[String])

object BookShow {
  implicit val showView = Format(Json.reads[BookShow], Json.writes[BookShow])
}