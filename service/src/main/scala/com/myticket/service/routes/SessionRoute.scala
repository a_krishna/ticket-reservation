package com.myticket.service.routes

import java.util.UUID

import akka.actor.ActorRef
import akka.util.Timeout
import com.myticket.core.domain._
import com.myticket.core.repo.MongoUserRepo
import com.myticket.service.Settings
import com.myticket.service.domain._
import akka.pattern.ask
import org.joda.time._
import spray.http.{ DateTime ⇒ SprayDateTime, _ }
import spray.httpx.PlayJsonSupport
import spray.routing.AuthenticationFailedRejection.CredentialsRejected
import spray.routing.{ AuthenticationFailedRejection, Directives, ExceptionHandler }
import spray.routing.authentication.{ Authentication, ContextAuthenticator }
import spray.util.LoggingContext

import scala.concurrent.{ ExecutionContext, Future }
import scala.util.control.NonFatal

trait SessionsRoute extends Directives with PlayJsonSupport {

  def sessionsRoute(sessionAuthenticator: ContextAuthenticator[Session], userRepo: MongoUserRepo, sessionCacheActor: ActorRef)(implicit ec: ExecutionContext, timeout: Timeout, log: LoggingContext) =
    pathPrefix("sessions") {
      path(JavaUUID ~ Slash.?) { sessionId ⇒
        get {
          complete {
            (sessionCacheActor ? GetSession(sessionId)).mapTo[Option[Session]]
          }
        }
      } ~
        (path("me" ~ Slash.?) & authenticate(sessionAuthenticator)) { session ⇒
          validate(session.id != Settings.apiSessionId, "API session cannot be accessed in this way") {
            get {
              complete {
                session
              }
            } ~
              delete {
                hostName { host ⇒
                  deleteCookie(name = "session", domain = host, path = "/") {
                    complete {
                      sessionCacheActor ! DeleteSession(session.id)
                      StatusCodes.OK
                    }
                  }
                }
              }
          }
        } ~
        //Login by posting username and password to this endpoint
        pathEndOrSingleSlash {
          (post & entity(as[Credentials])) { credentials ⇒
            handleExceptions(redactedCredentialsExceptionHandler(credentials.userName)) { //Replace default exception handler which would log the sensitive credentials
              authenticate(userAuthenticator(credentials, userRepo)) { user ⇒
                hostName { host ⇒ //Header value is set by nginx and used to explicitly set cookie's domain, so that the cookie works on all subdomains
                  createSessionRoute(user, sessionCacheActor, newSessionId = UUID.randomUUID, host)
                }
              }
            }
          }
        }
    }

  protected def createSessionRoute(user: User, sessionCacheActor: ActorRef, newSessionId: UUID, hostName: String) = {
    //Setting the expiration date's seconds to 0 so that it won't change before the unit test checks if it is set
    val cookieExpirationDate = SprayDateTime(DateTime.now().hourOfDay.roundFloorCopy.plus(Settings.cookieExpiration.toMillis).getMillis)

    (respondWithHeader(HttpHeaders.Location(Uri./ + "sessions/" + newSessionId)) &
      setCookie(HttpCookie(name = "session", content = newSessionId.toString, expires = Some(cookieExpirationDate),
        domain = Some(hostName), path = Some("/"), secure = true /*, httpOnly = true*/ ))) {
        complete {
          sessionCacheActor ! CreateSession(newSessionId, user, Settings.sessionExpiration)
          StatusCodes.OK
        }
      }
  }

  private def userAuthenticator(credentials: Credentials, userRepo: MongoUserRepo)(implicit ec: ExecutionContext): Future[Authentication[User]] =
    Future(userRepo.getByCredentials(credentials)).map(_.toRight(left = AuthenticationFailedRejection(CredentialsRejected, Nil)))

  private def redactedCredentialsExceptionHandler(username: String)(implicit log: LoggingContext) = ExceptionHandler {
    case NonFatal(e) ⇒
      log.error(e, s"Error during login attempt for '$username'")
      complete(StatusCodes.InternalServerError)
  }

}
