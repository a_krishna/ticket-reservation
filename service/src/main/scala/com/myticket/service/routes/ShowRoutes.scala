package com.myticket.service.routes

import com.myticket.core.domain.{ Admin, Anonymous, IMDB, NormalUser }
import com.myticket.service.api.TicketService
import com.myticket.service.domain.{ CreateShow, Session }
import org.joda.time.{ DateTime, DateTimeZone }
import spray.http.StatusCodes
import spray.httpx.PlayJsonSupport
import spray.routing.Directives
import spray.routing.authentication.ContextAuthenticator
import spray.util.LoggingContext

import scala.concurrent.{ ExecutionContext, Future }
import scala.util.{ Failure, Success }

trait ShowRoutes extends Directives with DomainDirectives with PlayJsonSupport {

  import SParams._

  def showRoute(authenticator: ContextAuthenticator[Session], ticketService: TicketService)(implicit ec: ExecutionContext, log: LoggingContext) =
    (pathPrefix("show") & authenticate(authenticator)) { session ⇒
      pathEndOrSingleSlash {
        get {
          byTimeAndMovie { (mov, time) ⇒
            (mov, time) match {
              case (Some(mv), None) ⇒ complete(
                StatusCodes.OK,
                ticketService.getShowByMovie(mv))
              case (None, Some(tm)) if tm >= now ⇒ complete(
                ticketService.getShowByTime(tm))
              case (None, Some(tm)) ⇒ complete(
                StatusCodes.BadRequest, "invalid time")

              case _ ⇒ complete(StatusCodes.BadRequest, "invalid request")
            }
          }
        }
      } ~ path("create" ~ Slash.?) {
        (authorize(isAdmin(session))) {
          post {
            (entity(as[CreateShow])) { createShow ⇒
              onComplete(ticketService.validateShow(createShow)) {
                case Success(show) ⇒ show match {
                  case Some(exists) ⇒ complete(StatusCodes.PreconditionFailed, "Show Already Exist")
                  case None ⇒ complete {
                    Future {
                      ticketService.createShow(createShow)
                    }
                  }
                }
                case Failure(e) ⇒ complete(StatusCodes.NoContent, e.getMessage)
              }

            }
          }
        }
      } ~ path("theatre" ~ Slash.?) {
        (authorize(isAdmin(session))) {
          get {
            complete(StatusCodes.OK, ticketService.getTheatres())
          }
        }
      } ~ path("imdb" ~ Slash.?) {
        (authorize(isAdmin(session))) {
          get {
            complete(StatusCodes.OK, ticketService.getMovies())
          } ~ post {
            (entity(as[IMDB])) { imdb ⇒
              complete {
                Future {
                  ticketService.addMovies(imdb)
                  StatusCodes.OK
                }
              }

            }
          }
        }
      }
    }

  private def isAdmin(session: Session): Boolean = session.user.role == Admin

  private def isUser(session: Session): Boolean = session.user.role == NormalUser

  private def isNonUser(session: Session): Boolean = session.user.role == Anonymous

  object SParams {
    def showByTime = 'time.as[Long]
    def showByMovie = 'movie.as[String]

    val byTimeAndMovie = parameters(showByMovie.?, showByTime.?)
    def now = DateTime.now(DateTimeZone.UTC).getMillis
  }

}
