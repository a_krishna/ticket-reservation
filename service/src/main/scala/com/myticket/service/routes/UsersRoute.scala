package com.myticket.service.routes

import akka.actor.ActorRef
import com.myticket.core.repo.MongoUserRepo
import com.myticket.core.domain._
import com.myticket.core.service.{ PasswordService, ValidationError }
import com.myticket.service.domain._
import org.joda.time.{ DateTime, DateTimeZone }
import play.api.libs.json._
import spray.http.Uri.Path
import spray.http.{ HttpHeaders, StatusCodes, Uri }
import spray.httpx.PlayJsonSupport
import spray.routing._
import spray.routing.authentication.ContextAuthenticator
import spray.util.LoggingContext

import scala.concurrent.{ ExecutionContext, Future }
import scala.util.control.NonFatal

trait UsersRoute extends Directives with DomainDirectives with PlayJsonSupport {

  def usersRoute(authenticator: ContextAuthenticator[Session], userRepo: MongoUserRepo, sessionCacheActor: ActorRef)(implicit ec: ExecutionContext, log: LoggingContext) =
    (pathPrefix("users")) {
      authenticate(authenticator) { session ⇒
        (pathEndOrSingleSlash & authorize(isAdmin(session))) {
          get {
            complete {
              Future {
                userRepo.getAll
              }
            }
          }
        } ~
          path(MongoObjectId ~ Slash.?) { userId ⇒
            asyncFind(userRepo.get(userId.toString)) { user ⇒
              val isUserAdmin = isUser(session) || isAdmin(session)

              authorize(isUserAdmin || session.user.id == user.id) { //Either a user admin can modify a user in his org, or a user can modify (parts of) himself
                get {
                  complete {
                    user
                  }
                } ~
                  (patch | post) {
                    entity(as[Credentials]) { credentials ⇒
                      updateCredentialsRoute(user, credentials, userRepo, sessionCacheActor)
                    } ~
                      entity(as[PartialUser]) { //TODO: non admins should be able to modify everything except permissions/access?
                        domainValidate(_) { updates ⇒

                          (validate(isUser(session) || isAdmin(session), "Only user or admin can update status")) {
                            complete {
                              Future {
                                userRepo.update(user.id, updates.copy(updatedOn = Some(DateTime.now(DateTimeZone.UTC).getMillis)))
                                sessionCacheActor ! RefreshSessions(user.id)
                                //                          dispatcher(UserUpdated(user.id, updates))
                                StatusCodes.OK
                              }
                            }
                          }
                        }
                      }
                  }
              } ~
                (delete) {
                  complete {
                    Future {
                      userRepo.delete(user.id)
                      sessionCacheActor ! RefreshSessions(user.id)
                      //                dispatcher(UserDeleted(user.id))
                      StatusCodes.OK
                    }
                  }
                }
            }
          }

      } ~
        path("create" ~ Slash.?) {
          post {
            //Create a new user from the input data
            implicit val reader = User.credentialHashingReader

            (validatePassword & entity(as[User])) { //Validate the user's password before it's hashed into the User.hashedPassword field
              domainValidate(_) { validatedUser ⇒
                requestUri { uri ⇒
                  val newUserPath = Path./(uri.path.tail.head.toString)./(validatedUser.id.toString)

                  respondWithHeader(HttpHeaders.Location(Uri(path = newUserPath))) {
                    complete {
                      Future {
                        userRepo.insert(validatedUser)
                        //                        dispatcher(UserCreated(validatedUser.id, validatedUser.copy(hashedPassword = "*****", salt = "*****")))
                        //Duplicate location header into request body because front-end guys don't like standards </passive aggressiveness>
                        (StatusCodes.Created, Json.obj("id" -> validatedUser.id))
                      }
                    }
                  }
                }
              }
            }
          }
        }

    }

  protected def updateCredentialsRoute(user: User, credentials: Credentials, userRepo: MongoUserRepo, sessionCacheActor: ActorRef)(implicit ec: ExecutionContext) =
    handleExceptions(redactedCredentialsExceptionHandler(user.name)) {
      domainValidate(credentials) { validatedCredentials ⇒
        complete {
          Future {
            userRepo.updateCredentials(user.id, validatedCredentials)
            sessionCacheActor ! RefreshSessions(user.id)
            //            dispatcher(UserCredentialsUpdated(user.id))
            StatusCodes.OK
          }
        }
      }
    }

  private def validatePassword: Directive0 =
    entity(as[JsObject]).flatMap {
      _ \ "password" match {
        case JsString(password) if PasswordService.isValid(password) ⇒ pass
        case _ ⇒ complete((StatusCodes.BadRequest, Seq(ValidationError("UserPasswordInvalid"))))
      }
    }

  private def redactedCredentialsExceptionHandler(username: String)(implicit log: LoggingContext) = ExceptionHandler {
    case NonFatal(e) ⇒
      log.error(e, s"Error during credentials update for '$username'")
      complete(StatusCodes.InternalServerError)
  }

  private def isAdmin(session: Session): Boolean = session.user.role == Admin

  private def isUser(session: Session): Boolean = session.user.role == NormalUser

  private def isNonUser(session: Session): Boolean = session.user.role == Anonymous

}
