package com.myticket.service.routes

import com.myticket.core.domain.{ Admin, NormalUser }
import com.myticket.service.api.TicketService
import com.myticket.service.domain.{ BookShow, Session }
import play.api.libs.json.Json
import spray.http.StatusCodes
import spray.httpx.PlayJsonSupport
import spray.routing.Directives
import spray.routing.authentication.ContextAuthenticator
import spray.util.LoggingContext

import scala.concurrent.{ ExecutionContext, Future }
import scala.util.{ Failure, Success }

trait TicketRoutes extends Directives with DomainDirectives with PlayJsonSupport {

  import TParams._

  def ticketRoute(authenticator: ContextAuthenticator[Session], ticketService: TicketService)(implicit ec: ExecutionContext, log: LoggingContext) =
    (pathPrefix("myticket") & authenticate(authenticator)) { session ⇒
      (pathEndOrSingleSlash & authorize(adminOrUser(session))) {
        get {
          OrderParams { (o, t) ⇒
            complete {
              (o, t) match {
                case (Some(or), None) ⇒
                  ticketService.getOrderById(or)
                case (None, Some(tk)) ⇒
                  ticketService.getOrder(tk)
                case _ ⇒ StatusCodes.BadRequest
              }
            }
          }

        } ~ post {
          entity(as[BookShow]) { bookShow ⇒
            complete {
              Future {
                ticketService.bookShow(bookShow.showId, session.user, bookShow.seats.toSet)
              }
            }
          }
        }
      } ~ path("purchase" ~ Slash.?) {
        onComplete(ticketService.purchaseTicket(session.user)) {
          case Success(data) ⇒ complete(data)
          case Failure(err)  ⇒ complete(StatusCodes.Conflict, s"Conflict While Purchase ${err.getMessage}")
        }
      }
    }

  private def isAdmin(session: Session): Boolean = session.user.role == Admin

  private def isUser(session: Session): Boolean = session.user.role == NormalUser

  def adminOrUser(session: Session) = isAdmin(session) || isUser(session)

  object TParams {
    def byOrderId = 'order.as[String]
    def byTicketId = 'ticketId.as[String]

    val OrderParams = parameters(byOrderId.?, byTicketId.?)
  }
}
