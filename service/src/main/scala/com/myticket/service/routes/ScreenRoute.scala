package com.myticket.service.routes

import com.myticket.core.domain.PartialTheatre
import com.myticket.service.api.TheatreService
import com.myticket.service.domain.{ Session, TheatreScreen }
import spray.http.StatusCodes
import spray.httpx.PlayJsonSupport
import spray.routing.Directives
import spray.routing.authentication.ContextAuthenticator
import spray.util.LoggingContext

import scala.concurrent.ExecutionContext
import scala.util.{ Failure, Success }

trait ScreenRoute extends Directives with DomainDirectives with PlayJsonSupport {
  //  import ScreenMarshaller._
  import Params._

  def screenRoute(authenticator: ContextAuthenticator[Session], theatreService: TheatreService)(implicit ec: ExecutionContext, log: LoggingContext) =
    (pathPrefix("theatre") & authenticate(authenticator)) { session ⇒
      pathEndOrSingleSlash {
        get {
          complete {
            theatreService.getTheatre()
          }
        } ~ post {
          (entity(as[PartialTheatre])) { partialTheatre ⇒
            pass { ctx ⇒
              theatreService.createTheatre(partialTheatre).onComplete {
                case Success(result) ⇒ result.fold(
                  fail ⇒ ctx.complete(StatusCodes.BadRequest, fail.list),
                  succ ⇒ ctx.complete(StatusCodes.Created, succ))
                case Failure(err) ⇒ ctx.failWith(err)
              }
            }
          }
        }
      } ~ path("screen" ~ Slash.?) {
        get {
          queryByTheatreAndScreen { (t, s) ⇒
            theatreService.addScreensToTheatre(t, Set(s)).onComplete {
              case Success(result) ⇒ result.fold(
                fail ⇒ complete(StatusCodes.BadRequest, fail.list),
                succ ⇒ complete(StatusCodes.Created))
              case Failure(err) ⇒ failWith(err)
            }
            complete(StatusCodes.OK)
          } ~ queryByName { (t, s) ⇒
            (t, s) match {
              case (Some(tN), None) ⇒ complete(StatusCodes.OK, theatreService.getScreenByTheatre(tN))
              case (None, Some(sN)) ⇒ complete(StatusCodes.OK, theatreService.getByScreenName(sN))
              case _                ⇒ complete(StatusCodes.BadGateway)
            }
          }
        } ~ post {
          (entity(as[TheatreScreen])) { tS ⇒
            pass { ctx ⇒
              theatreService.createScreen(tS).onComplete {
                case Success(result) ⇒ result.fold(
                  fail ⇒ ctx.complete(StatusCodes.BadRequest, fail.list),
                  succ ⇒ ctx.complete(StatusCodes.Created, succ))
                case Failure(err) ⇒ ctx.failWith(err)
              }
            }

          }
        }
      }
    }

  object Params {
    def theatre = 'theatreId.as[String]

    def screen = 'screenId.as[String]

    val queryByTheatreAndScreen = parameters(theatre, screen)
    val queryByName = parameters('theatreName.as[String].?, 'screenName.as[String].?)
  }
}
//object ScreenMarshaller {
//  import play.api.libs.functional.syntax._
//  import play.api.libs.json._
//  import com.myticket.core.domain._
//
//  implicit class PlaytoSprayJsonMarshaller[T](self: Writes[T]) {
//    def asMarshaller: Marshaller[T] = Marshaller.delegate[T, String](ContentTypes.`application/json`) { value ⇒ Json.stringify(self.writes(value)) }
//
//    def asTraversableMarshaller: Marshaller[Traversable[T]] = Writes.traversableWrites(self).asMarshaller
//  }
//
//  implicit val tS = Format(Json.reads[SeatSpecification], Json.writes[SeatSpecification])
//
//  implicit val seatsSpec = Format(Json.reads[TheatreScreen], Json.writes[TheatreScreen])
//
//  implicit val seats = Format(Json.reads[Seat], Json.writes[Seat])
//
//  implicit val matTS = seatsSpec.asMarshaller
//
//  implicit val mattS = tS.asMarshaller
//
//}