package com.myticket.service.routes

import com.myticket.core.service.ValidationService
import com.myticket.core.service.ValidationService.Validator
import spray.http.StatusCodes
import spray.httpx.PlayJsonSupport
import spray.routing.Directive1
import spray.routing.Directives.{ complete, onSuccess, provide, validate }

import scala.concurrent.{ ExecutionContext, Future }
import scalaz.{ Failure, Success }

trait DomainDirectives {

  import DomainDirectives._

  def domainValidate[T](magnet: DomainValidateMagnet[T]): Directive1[T] = magnet()

  def asyncFind[T](magnet: AsyncFindMagnet[T]): Directive1[T] = magnet()

  def requireOption[T](option: Option[T], errorMsg: String): Directive1[T] =
    validate(option.isDefined, errorMsg).hflatMap(_ ⇒ provide(option.get))

}

object DomainDirectives extends PlayJsonSupport {

  implicit class DomainValidateMagnet[T: Validator](validatable: T) {
    def apply(): Directive1[T] =
      ValidationService.validate(validatable) match {
        case Success(validated) ⇒ provide(validated)
        case Failure(errors)    ⇒ complete((StatusCodes.BadRequest, errors.list))
      }
  }

  implicit class AsyncFindMagnet[T](finder: ⇒ Option[T])(implicit ec: ExecutionContext) {
    def apply(): Directive1[T] =
      onSuccess(Future(finder)).flatMap {
        case Some(entity) ⇒ provide(entity)
        case None         ⇒ complete(StatusCodes.NotFound)
      }
  }

}