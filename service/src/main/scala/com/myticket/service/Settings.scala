package com.myticket.service

import java.net.InetAddress
import java.util.UUID

import akka.util.Timeout
import com.mongodb.casbah.MongoClientURI
import com.typesafe.config.{ Config, ConfigFactory }

class Settings(val config: Config) {

  val myticketConfig = config.getConfig("myticket")
  import myticketConfig._

  val host = if (hasPath("host")) getString("host") else InetAddress.getLoopbackAddress.getHostAddress
  val port = getInt("port")
  val webServiceTimeout = Timeout(getFiniteDuration("web-service-timeout"))
  val sessionExpiration = getFiniteDuration("session-expiration")
  val apiSessionId = UUID.fromString(getString("api-session-id"))
  val cookieExpiration = getFiniteDuration("cookie-expiration")

  val dbUri = MongoClientURI(getString("db.uri"))
  val dbName = getString("db.name")

  //Helper methods
  def getFiniteDuration(path: String) = {
    import scala.concurrent.duration._
    getDuration(path, java.util.concurrent.TimeUnit.MILLISECONDS).millis
  }

  def getCfgList(path: String) = {
    import scala.collection.JavaConverters._
    getConfigList(path).asScala.toList
  }

}

object Settings extends Settings(ConfigFactory.load)

