package com.myticket.service.api

import java.util.UUID

import akka.actor.ActorSystem
import com.myticket.core.domain.User
import com.myticket.service.domain.{ GetSession, Session }
import org.joda.time.DateTime
import spray.routing.{ AuthenticationFailedRejection, MissingCookieRejection }
import spray.routing.authentication.ContextAuthenticator
import akka.pattern._
import akka.util.Timeout

import scala.concurrent.{ ExecutionContext, Future }

object SessionService {

  def fakeSessionAuthenticator(sessionIdToUser: Map[UUID, User]): ContextAuthenticator[Session] =
    ctx ⇒ {
      val headerSessionIdOption = ctx.request.headers.find(_.name.equalsIgnoreCase("session")).map(_.value)
      val cookieSessionIdOption = ctx.request.cookies.find(_.name.equalsIgnoreCase("session")).map(_.content)
      val sessionIdOption = (headerSessionIdOption orElse cookieSessionIdOption).map(UUID.fromString)

      Future.successful {
        for {
          sessionId ← sessionIdOption.toRight(left = MissingCookieRejection("session")).right
          user ← sessionIdToUser.get(sessionId)

            .toRight(left = AuthenticationFailedRejection(AuthenticationFailedRejection.CredentialsRejected, Nil)).right
        } yield Session(id = sessionId, createdOn = DateTime.now(), touchedOn = DateTime.now(), user = user)
      }
    }

  //  def sessionAuthenticator(system: ActorSystem)(implicit executionContext: ExecutionContext, timeout: Timeout): ContextAuthenticator[Session] =
  //    ctx ⇒ {
  //      val headerSessionIdOption = ctx.request.headers.find(_.name.equalsIgnoreCase("session")).map(_.value)
  //      val cookieSessionIdOption = ctx.request.cookies.find(_.name.equalsIgnoreCase("session")).map(_.content)
  //      val sessionIdOption = (headerSessionIdOption orElse cookieSessionIdOption).map(UUID.fromString)
  //
  //      Future {
  //        for {
  //          sessionId ← sessionIdOption
  //          session <- (system.actorSelection("sessions") ? GetSession(sessionId)).mapTo[Session].map(x=> Some(x))
  //          _data <- session.toRight(left = MissingCookieRejection("session")).right
  //             user <- Some(_data)
  //               .toRight(left = AuthenticationFailedRejection(AuthenticationFailedRejection.CredentialsRejected, Nil)).right
  //        } yield user
  //      }
  //    }
}
