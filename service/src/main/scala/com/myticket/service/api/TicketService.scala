package com.myticket.service.api

import com.myticket.core.ObjectId
import com.myticket.core.domain._
import com.myticket.core.repo._
import com.myticket.service.actors.EventService
import com.myticket.service.domain._
import org.joda.time.{ DateTime, DateTimeZone }

import scala.concurrent.{ ExecutionContext, Future, blocking }

trait TicketService {

  def getTheatres(): Seq[Theatre]
  def createShow(createShow: CreateShow): Future[Option[Show]]
  def getShowByMovie(movieName: String): List[ShowView]
  def getShowByMovieAndTheatre(movieName: String, theatreId: String): List[ShowView]
  def getShowByTime(time: Long): List[ShowView]
  def getOrder(ticketId: String): Option[Ticket]
  def getOrderById(orderId: String): Option[Order]
  def bookShow(showId: String, user: User, seats: Set[String]): Future[Option[Ticket]]
  def purchaseTicket(user: User): Future[Option[Ticket]]
  def validateShow(createShow: CreateShow): Future[Option[String]]
  def addMovies(iMDB: IMDB)
  def verifyReturn(ticketId: String): Future[Option[Boolean]]
  def getMovies(): Seq[IMDB]

}
class TicketServiceImpl(screenRepo: ScreenRepo, iMDBRepo: IMDBRepo, theatreRepo: TheatreRepo, showRepo: ShowRepo, orderRepo: OrderRepo, userRepo: MongoUserRepo, cardRepo: MongoCardRepo, eventService: EventService[BookingEvent], ec: ExecutionContext) extends TicketService {
  implicit def executionContext = ec

  def currentDayInMillis = DateTime.now(DateTimeZone.UTC).getMillis
  override def getTheatres(): Seq[Theatre] = theatreRepo.getAll

  override def createShow(createShow: CreateShow): Future[Option[Show]] = Future(blocking {
    for {
      imdb ← iMDBRepo.get(createShow.imdbId)
      theatre ← theatreRepo.get(createShow.theatreId)
      screen ← screenRepo.get(createShow.screenId)
      show = generateShow(imdb, theatre, screen, createShow)
      // publish eventStream
    } yield show
  })

  override def getShowByMovie(movieName: String): List[ShowView] = {
    val imdb = iMDBRepo.getByMovieName(movieName)
    imdb.fold(List.empty[ShowView]) { im ⇒
      val show = showRepo.getByMovie(im.id).filter(_.time > TimeService.beginningOfDay(currentDayInMillis))
      generateShowView(im, show)
    }

  }

  override def getShowByMovieAndTheatre(movieName: String, theatreId: String): List[ShowView] = getShowByMovie(movieName)

  override def getShowByTime(time: Long): List[ShowView] = {
    val show = showRepo.getByTime(TimeService.beginningOfDay(time), TimeService.endOfTheDay(time))
    show.map { z ⇒
      genrateShowView(z)
    }
  }

  override def getOrder(ticketId: String): Option[Ticket] = {
    val orderOp = orderRepo.getByTicketId(ticketId)
    orderOp.flatMap { order ⇒
      generateTicket(order)
    }
  }

  override def getOrderById(orderId: String): Option[Order] = orderRepo.get(orderId)

  override def bookShow(showId: String, user: User, seats: Set[String]): Future[Option[Ticket]] = Future(blocking {
    for {
      show ← showRepo.get(showId)
      cardId ← user.defaultCardId
      screen ← screenRepo.get(show.screenId)
      order = generateOrder(screen, cardId, user.id, show, seats)
      ticket ← generateTicket(order)
      tc = saveAndPublish(ticket, order)
    } yield tc
  })

  import SeatHelper._

  private def saveAndPublish(ticket: Ticket, order: Order): Ticket = {
    orderRepo.save(order.copy(ticketId = Some(ticket.ticketId), status = Some(ticket.status)))
    eventService.publish(ticket)
    ticket
  }
  private def generateTicket(order: Order): Option[Ticket] = {
    val showOp = showRepo.get(order.showId)
    val ticket = Ticket(ticketId = order.ticketId.getOrElse(""), userId = Some(order.userId), seats = order.seats.map(_.seatName), price = order.totalPrice, status = order.status.getOrElse(Rejected))
    showOp.map { show ⇒
      val showView = genrateShowView(show)
      ticket.copy(movieName = Some(showView.movieName), showTime = Some(showView.timing), screenName = showView.screenName)
    }
  }

  private def generateShow(iMDB: IMDB, theatre: Theatre, screen: Screen, createShow: CreateShow): Show = {
    val generatedSeat = generateSeatNum(createShow.row, createShow.column)
    val allotedSeats = screen.seatsId.filter(z ⇒ generatedSeat.contains(z.seatName)).map(x ⇒ x.copy(price = x.price + screen.priceAddOn))
    val show = Show(imdbId = iMDB.id, theatreId = theatre.id, screenId = screen.id, availableSeatsId = allotedSeats, time = createShow.time)
    showRepo.save(show)
    show
  }

  private def generateShowView(iMDB: IMDB, shows: List[Show]): List[ShowView] =
    shows.map(z ⇒ ShowView(z.id, iMDB.movieName, theatreView(theatreRepo.get(z.theatreId)), screenRepo.get(z.screenId).map(_.screenName), z.time, z.availableSeatsId.toList.sortBy(_.seatName), z.reservedSeatsId.toList.sortBy(_.seatName)))

  private def genrateShowView(show: Show): ShowView =
    ShowView(show.id, iMDBRepo.get(show.imdbId).map(_.movieName).getOrElse("None"), theatreView(theatreRepo.get(show.theatreId)), screenRepo.get(show.screenId).map(_.screenName), show.time, show.availableSeatsId.toList, show.reservedSeatsId.toList)

  private def theatreView(theatre: Option[Theatre]): Option[TheatreView] = theatre match {
    case Some(th) ⇒ Some(TheatreView(th.name, th.city, th.state, th.country))
    case None     ⇒ None
  }

  private def generateOrder(screen: Screen, cardId: String, userId: String, show: Show, seats: Set[String]): Order = {
    val availableAfterBooking = show.availableSeatsId.filterNot(z ⇒ seats.contains(z.seatName))
    val bookable = show.availableSeatsId.filter(z ⇒ seats.contains(z.seatName))
    if (availableAfterBooking.isEmpty || !bookable.map(_.seatName).equals(seats)) {
      val order = Order(showId = show.id, cardId = cardId, userId = userId, totalPrice = 0.0D, status = Some(NotAvailable))
      orderRepo.save(order)
      order
    }
    else {
      showRepo.save(show.copy(availableSeatsId = availableAfterBooking, reservedSeatsId = show.reservedSeatsId ++ bookable))
      val order = Order(showId = show.id, cardId = cardId, userId = userId, totalPrice = bookable.toList.map(_.price).sum, status = Some(Pending), seats = bookable)
      orderRepo.save(order)
      order
    }
  }

  def verifyReturn(ticketId: String): Future[Option[Boolean]] = Future(blocking {
    for {
      order ← orderRepo.getByTicketId(ticketId)
      show ← showRepo.get(order.showId)
      res = updateShowSeats(show, order)
    } yield res
  })

  private def updateShowSeats(show: Show, order: Order) = {
    order.status.getOrElse(Cancel) match {
      case Pending ⇒
        val seats = order.seats.map(_.seatName)
        val availableReservedSeats = show.reservedSeatsId.filterNot(z ⇒ seats.contains(z.seatName))
        showRepo.save(show.copy(availableSeatsId = show.availableSeatsId ++ order.seats, reservedSeatsId = availableReservedSeats))
        orderRepo.save(order.copy(status = Some(Cancel)))
        true
      case _ ⇒ false
    }
  }

  private def purchasedTicket(order: Order): Option[Ticket] = {
    generateTicket(order.copy(status = Some(Booked))).map(t ⇒ t.copy(ticketId = "MYTIC" + ObjectId()))
  }

  override def purchaseTicket(user: User): Future[Option[Ticket]] = Future(blocking {
    for {
      order ← orderRepo.getByUser(user)
      //      card <- cardRepo.getByBalance(user.defaultCardId.getOrElse(""),order.totalPrice)
      ticket ← purchasedTicket(order)
      tc = saveAndPublish(ticket, order)
    } yield tc
  })

  override def validateShow(createShow: CreateShow): Future[Option[String]] = Future(
    for {
      data ← showRepo.recordExcist(createShow.imdbId, createShow.theatreId, createShow.screenId, createShow.time)
      res = "exist"
    } yield res)
  override def addMovies(iMDB: IMDB): Unit = iMDBRepo.save(iMDB)

  override def getMovies(): Seq[IMDB] = iMDBRepo.getAll
}