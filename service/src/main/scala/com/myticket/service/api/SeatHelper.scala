package com.myticket.service.api

import com.myticket.core.domain.Seat

object SeatHelper {

  private def baseAlpha = (0 to 25).map(_ + 97).map(_.asInstanceOf[Char].toString)
  private def base2Alpha = baseAlpha.map(z ⇒ (1 to 2).map(x ⇒ z).mkString).toSet

  //Generating SeatNum Based on User Requirement with Class of Seat based on Price
  def generateSeat(seats: List[(String, Double, Int, Int)]) = {
    val alpha = baseAlpha ++ base2Alpha
    seats.sortBy(_._2).foldLeft((alpha, List.empty[Seat]))((z, a) ⇒ (z._1.drop(a._3), z._2 ++ seatNumGen(z._1.take(a._3), a._4, a._1, a._2)))._2
  }

  def generateSeatNum(row: Int, column: Int) = {
    val alpha = baseAlpha ++ base2Alpha
    alpha.take(row).foldLeft(List.empty[String])((z, a) ⇒ z ++ (1 to column).map(x ⇒ a + x))
  }

  private def seatNumGen(row: IndexedSeq[String], column: Int, _type: String, price: Double): List[Seat] = row.map(z ⇒ (1 to column).map(x ⇒ Seat(z + x, _type, price))).flatten.toList
}
