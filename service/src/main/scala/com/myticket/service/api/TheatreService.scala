package com.myticket.service.api

import com.myticket.core.domain._
import com.myticket.core.repo.{ ScreenRepo, TheatreRepo }
import com.myticket.core.service.ValidationService.Validated
import com.myticket.service.domain.TheatreScreen

import scala.concurrent.{ ExecutionContext, Future, blocking }
import scalaz.Scalaz._
import scalaz.Validation.FlatMap._

trait TheatreService {

  def createScreen(theatreScreen: TheatreScreen): Future[Validated[String]]
  def createTheatre(partialTheatre: PartialTheatre): Future[Validated[String]]
  def addScreensToTheatre(theatreId: String, screenId: Set[String]): Future[Validated[Unit]]
  def getAvailableScreen(): Future[Validated[List[Screen]]]
  def getTheatre(): List[Theatre]
  def getByTheatreName(name: String): Option[Theatre]
  def getByScreenName(name: String): Option[Screen]
  def getScreenByTheatre(name: String): Option[List[Screen]]
  //  def getSeatsByTheatreAndScreen(theatreId:String,screenId:String):Set[Seat]
}
class TheatreServiceImpl(screenRepo: ScreenRepo, theatreRepo: TheatreRepo, executionContext: ExecutionContext) extends TheatreService {
  import com.myticket.core.service.ValidationService._

  private implicit val ec = executionContext

  override def createScreen(theatreScreen: TheatreScreen): Future[Validated[String]] = Future(blocking {
    for {
      vD ← validate(generateScreen(theatreScreen))
      data = PartialScreen.copyScreen(vD)
      _ ← screenRepo.save(data).successNel
    } yield data.id
  })

  override def createTheatre(partialTheatre: PartialTheatre): Future[Validated[String]] = Future(blocking {
    for {
      _ ← validate(partialTheatre)
      data = PartialTheatre.copyTheatre(partialTheatre)
      _ ← theatreRepo.save(data).successNel
    } yield data.id
  })

  override def addScreensToTheatre(theatreId: String, screenId: Set[String]): Future[Validated[Unit]] = Future(blocking {
    for {
      _ ← theatreRepo.addScreens(theatreId, screenId).successNel
    } yield ().successNel
  })

  override def getAvailableScreen(): Future[Validated[List[Screen]]] = Future(blocking {
    for {
      used ← theatreRepo.getAll.toList.map(_.screenIds).flatten.successNel
      screens = screenRepo.getAll.toList.filterNot(z ⇒ used.contains(z.id))
    } yield screens
  })

  override def getTheatre(): List[Theatre] = theatreRepo.getAll.toList

  import SeatHelper._

  private def generateScreen(tS: TheatreScreen): PartialScreen = {
    val seats = tS.seats.map(z ⇒ (z.seatClass, z.price, z.row, z.column))
    PartialScreen(screenName = Some(tS.screenName), screenType = Some(tS.screenType), seatsId = Some(generateSeat(seats).toSet))
  }

  override def getByTheatreName(name: String): Option[Theatre] = theatreRepo.getByName(name)

  override def getByScreenName(name: String): Option[Screen] = screenRepo.getByScreenName(name)

  override def getScreenByTheatre(name: String): Option[List[Screen]] = theatreRepo.getByName(name).map { t ⇒
    screenRepo.getByIds(t.screenIds.toList).toList
  }
}
