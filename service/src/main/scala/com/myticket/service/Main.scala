package com.myticket.service

import akka.actor.{ ActorSystem, Props }
import com.mongodb.casbah.MongoClient
import com.myticket.core.ObjectId
import com.myticket.core.domain._
import com.myticket.core.repo._
import com.myticket.core.service.PasswordService
import com.novus.salat.global.ctx
import com.myticket.service.actors.{ AkkaEventService, SessionCacheActor, TicketWatcherManager, WebServiceActor }
import com.myticket.service.api.{ TheatreServiceImpl, TicketService, TicketServiceImpl }
import com.myticket.service.domain.BookingEvent
import com.typesafe.scalalogging.StrictLogging

object Main extends App with StrictLogging {

  val mongoClient = MongoClient(Settings.dbUri)

  implicit val actorSystem = ActorSystem("myticket-cluster")
  import actorSystem.dispatcher
  val userRepo = new MongoUserRepo(mongoClient, Settings.dbName)
  val cardRepo = new MongoCardRepo(mongoClient, Settings.dbName)
  val imdbRepo = new IMDBRepo(mongoClient, Settings.dbName)
  val theatreRepo = new TheatreRepo(mongoClient, Settings.dbName)
  val showRepo = new ShowRepo(mongoClient, Settings.dbName)
  val screenRepo = new ScreenRepo(mongoClient, Settings.dbName)
  val orderRepo = new OrderRepo(mongoClient, Settings.dbName)

  imdbRepo.ensureIndex()
  theatreRepo.ensureIndex()
  showRepo.ensureIndex()
  screenRepo.ensureIndex()
  //  orderRepo.ensureIndex()
  import LoadIntialData._
  //
  //  cardRepo.insert(card)
  //  cardRepo.insert(cardUser)
  //
  //  userRepo.insert(userAdmin)
  //  userRepo.insert(userRole)
  //
  //  screenRepo.save(screenOne)
  //  screenRepo.save(screenTwo)
  //  screenRepo.save(screenThree)
  //  screenRepo.save(screenfour)
  //
  //  theatreRepo.save(theatreOne)
  //  theatreRepo.save(theatreTwo)
  //  imdbRepo.save(imdb)
  //  imdbRepo.save(imdb1)
  //  imdbRepo.save(imdb2)
  //  imdbRepo.save(imdb3)
  //  imdbRepo.save(imdb4)

  val ticketService = new TicketServiceImpl(screenRepo = screenRepo, iMDBRepo = imdbRepo, theatreRepo = theatreRepo, userRepo = userRepo, cardRepo = cardRepo, showRepo = showRepo, orderRepo = orderRepo, eventService = new AkkaEventService[BookingEvent](), ec = actorSystem.dispatcher)
  val theatreService = new TheatreServiceImpl(screenRepo = screenRepo, theatreRepo = theatreRepo, executionContext = actorSystem.dispatcher)

  val sessionCacheActor = actorSystem.actorOf(Props(new SessionCacheActor(userRepo, metrics = true)), name = "sessions")

  actorSystem.actorOf(Props(new TicketWatcherManager(ticketService)), "ticket-manager")

  actorSystem.actorOf(Props(new WebServiceActor(Settings.host, Settings.port, userRepo, ticketService, theatreService, sessionCacheActor)), name = "web-service")

  WebServiceActor.groupUser.foreach(z ⇒ println(s"${z._1} ${z._2}"))
  sys.addShutdownHook {
    actorSystem.log.info("Shutting down")
    actorSystem.shutdown()
    actorSystem.awaitTermination()
    logger.info(s"Actor system '${actorSystem.name}' successfully shut down")

    logger.info("Shutting down mongo client")
    mongoClient.close()
    logger.info("Mongo client successfully shut down")

  }
}

object LoadIntialData {
  import com.myticket.service.api.SeatHelper._

  val saltUserAdmin = PasswordService.newSalt
  val saltUser = PasswordService.newSalt
  val card = Card(name = "AdminAccount", credit = 200000)
  val cardUser = card.copy(id = ObjectId(), name = "userrole")
  val userAdmin = User(name = "MyTicketAdmin", role = Admin, firstName = "Myticket", lastName = "AdminRole", email = Some("myticketUser@new.com"), salt = saltUserAdmin, hashedPassword = PasswordService.hashedPassword("1234567as", saltUserAdmin), defaultCardId = Some(card.id))
  val userRole = User(name = "TicketUser", role = NormalUser, firstName = "Ticket", lastName = "UserA", email = Some("ticket@new.com"), salt = saltUser, hashedPassword = PasswordService.hashedPassword("12345678", saltUser), defaultCardId = Some(cardUser.id))

  val seats = generateSeat(List(("SLIVER", 100.0D, 10, 10),
    ("GOLD", 150.0D, 4, 4), ("PLATINUM", 200.0D, 2, 2)))
  val screenOne = Screen(screenName = "SCREEN1", screenType = `3D`, seatsId = seats.toSet)
  val screenTwo = Screen(screenName = "SCREEN2", screenType = `2D`, seatsId = seats.toSet)
  val screenThree = Screen(screenName = "SCREEN3", screenType = IMAX, seatsId = seats.toSet)
  val screenfour = Screen(screenName = "SCREEN4", screenType = IMAX, seatsId = seats.toSet)

  val theatreOne = Theatre(name = "PVR", city = "CHENNAI", state = "TAMILNADU", country = "INDIA", screenIds = Set(screenOne.id, screenTwo.id))
  val theatreTwo = Theatre(name = "CINEMAX", city = "DELHI", state = "NEW DELHI", country = "INDIA", screenIds = Set(screenThree.id, screenfour.id))

  val imdb = IMDB(movieName = "AVATAR 2", casts = Map("Director" -> "James Cameroon"))
  val imdb1 = IMDB(movieName = "2.0", casts = Map("Director" -> "Shankar"))
  val imdb2 = IMDB(movieName = "BLADE RUNNER 2049", casts = Map("Director" -> "Denis Villeneuve"))
  val imdb3 = IMDB(movieName = "LOGAN", casts = Map("Director" -> "James Mangold"))
  val imdb4 = IMDB(movieName = "WONDER WOMEN", casts = Map("Director" -> "Patty Jenkins"))
}