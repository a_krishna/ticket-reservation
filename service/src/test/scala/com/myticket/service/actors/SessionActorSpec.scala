package com.myticket.service.actors

import java.util.UUID

import akka.testkit.{ TestActorRef, TestProbe }
import com.miguno.akka.testing.VirtualTime
import com.myticket.core.repo.MongoUserRepo
import com.myticket.service.{ ActorSpec, TestUtils }
import org.scalatest.mock.MockitoSugar

import scala.concurrent.duration._
import java.util.UUID

import com.myticket.service.domain._

class SessionActorSpec extends ActorSpec with MockitoSugar {

  "A session" must "expire" in {
    val time = new VirtualTime
    val sessionActor = TestActorRef(new SessionActor(user = TestUtils.fakeUser(), userRepo = mock[MongoUserRepo],
      expiration = 10.seconds, scheduler = time.scheduler, metrics = false), name = UUID.randomUUID.toString)
    val probe = TestProbe()
    probe.watch(sessionActor)

    time.advance(15.seconds)

    probe.expectTerminated(sessionActor)
  }

  it must "not expire too soon" in {
    val time = new VirtualTime
    val sessionActor = TestActorRef(new SessionActor(user = TestUtils.fakeUser(), userRepo = mock[MongoUserRepo],
      expiration = 10.seconds, scheduler = time.scheduler, metrics = false), name = UUID.randomUUID.toString)
    val probe = TestProbe()
    probe.watch(sessionActor)

    time.advance(9.seconds)

    probe.expectNoMsg(2.seconds)
  }

  it must "not expire if a user interacts with it" in {
    val time = new VirtualTime
    val sessionId = UUID.randomUUID
    val sessionActor = TestActorRef(new SessionActor(user = TestUtils.fakeUser(), userRepo = mock[MongoUserRepo],
      expiration = 10.seconds, scheduler = time.scheduler, metrics = false), name = sessionId.toString)
    val probe = TestProbe()
    probe.watch(sessionActor)

    time.advance(5.seconds)
    sessionActor ! GetSession(sessionId)
    time.advance(5.seconds)
    sessionActor ! GetSession(sessionId)
    time.advance(5.seconds)

    probe.expectNoMsg(2.seconds)
  }

  it must "not expire if its expiration time is infinite" in {
    val time = new VirtualTime
    val sessionId = UUID.randomUUID
    val sessionActor = TestActorRef(new SessionActor(user = TestUtils.fakeUser(), userRepo = mock[MongoUserRepo],
      expiration = Duration.Inf, scheduler = time.scheduler, metrics = false), name = sessionId.toString)
    val probe = TestProbe()
    probe.watch(sessionActor)

    time.advance(500.days)

    probe.expectNoMsg(2.seconds)
  }

}
