package com.myticket.service

import java.util.UUID

import com.myticket.core.ObjectId
import com.myticket.core.service.PasswordService
import com.myticket.core.domain._
import com.myticket.service.domain._
import org.joda.time.DateTime
import spray.routing.{ AuthenticationFailedRejection, MissingCookieRejection }
import spray.routing.authentication.ContextAuthenticator

import scala.concurrent.Future

object TestUtils {

  def fakeUser(password: String = "pa$$word") = {
    val salt = PasswordService.newSalt

    User(
      id = ObjectId(),
      firstName = "Joe",
      lastName = "Blow",
      name = "joeblow2",
      hashedPassword = PasswordService.hashedPassword(password, salt),
      salt = salt,
      role = Admin)
  }

  def fakeSessionAuthenticator(sessionIdToUser: Map[UUID, User]): ContextAuthenticator[Session] =
    ctx ⇒ {
      val headerSessionIdOption = ctx.request.headers.find(_.name.equalsIgnoreCase("session")).map(_.value)
      val cookieSessionIdOption = ctx.request.cookies.find(_.name.equalsIgnoreCase("session")).map(_.content)
      val sessionIdOption = (headerSessionIdOption orElse cookieSessionIdOption).map(UUID.fromString)

      Future.successful {
        for {
          sessionId ← sessionIdOption.toRight(left = MissingCookieRejection("session")).right
          user ← sessionIdToUser.get(sessionId)
            .toRight(left = AuthenticationFailedRejection(AuthenticationFailedRejection.CredentialsRejected, Nil)).right
        } yield Session(id = sessionId, createdOn = DateTime.now(), touchedOn = DateTime.now(), user = user)
      }
    }

}

