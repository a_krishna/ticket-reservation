package com.myticket.service.routes

import java.util.UUID

import akka.testkit.{ TestActorRef, TestProbe }
import akka.util.Timeout
import com.myticket.core.domain._
import com.myticket.core.repo.MongoUserRepo
import com.myticket.service.actors.SessionCacheActor
import com.myticket.service.domain._
import com.myticket.service.{ RouteSpec, Settings, TestUtils }
import org.joda.time.DateTime
import org.scalatest.mock.MockitoSugar
import play.api.libs.json.Json
import org.mockito.Mockito._
import spray.http.HttpHeaders.`Set-Cookie`
import spray.http.{ DateTime ⇒ SprayDateTime, HttpCookie, ContentTypes, StatusCodes }
import spray.httpx.PlayJsonSupport
import spray.routing.AuthenticationFailedRejection
import spray.routing.AuthenticationFailedRejection.CredentialsRejected
import spray.routing.authentication.ContextAuthenticator

import scala.concurrent.duration._

class SessionsRouteSpec extends RouteSpec with SessionsRoute with PlayJsonSupport with MockitoSugar {

  import com.myticket.service.actors.WebServiceActor.exceptionHandler

  val uri = "/sessions/"
  implicit val timeout: Timeout = 1.second
  implicit val credentialsWriter = Json.writes[Credentials] //We only use this here. In production we only use the reader

  "Sessions route" must "get a redacted session" in {
    val mockUserRepo = mock[MongoUserRepo]
    val fakeUser = TestUtils.fakeUser()
    val sessionId = UUID.randomUUID

    val sessionCacheActor = TestActorRef(new SessionCacheActor(mockUserRepo, metrics = false))
    sessionCacheActor ! CreateSession(sessionId, fakeUser, Settings.sessionExpiration)

    Get(uri + sessionId) ~> sessionsRoute(mock[ContextAuthenticator[Session]], mockUserRepo, sessionCacheActor) ~> check {
      val session = responseAs[Session]

      session.id mustBe sessionId
      session.user mustBe fakeUser.copy(hashedPassword = "*****", salt = "*****")
      contentType mustBe ContentTypes.`application/json`
      status mustBe StatusCodes.OK
    }
  }

  it must "allow a user to login with correct credentials" in {
    val mockUserRepo = mock[MongoUserRepo]
    val sessionCacheProbe = TestProbe()
    val fakeUser = TestUtils.fakeUser()
    val fakeCredentials = Credentials(userName = "foo", password = "bar")
    val cookieExpirationDate = SprayDateTime(DateTime.now().hourOfDay.roundFloorCopy.plus(Settings.cookieExpiration.toMillis).getMillis)

    when(mockUserRepo.getByCredentials(fakeCredentials)).thenReturn(Some(fakeUser))

    Post(uri, fakeCredentials) ~> sessionsRoute(mock[ContextAuthenticator[Session]], mockUserRepo, sessionCacheProbe.ref) ~> check {
      val newSessionId = sessionCacheProbe.expectMsgPF() { case CreateSession(sessionId, user, Settings.sessionExpiration) if user == fakeUser ⇒ sessionId }

      header[`Set-Cookie`].value mustBe `Set-Cookie`(HttpCookie(name = "session", content = newSessionId.toString,
        expires = Some(cookieExpirationDate), domain = Some("example.com"), path = Some("/"), secure = true /*, httpOnly = true*/ ))
      status mustBe StatusCodes.OK
    }
  }

  it must "not allow a user to login with incorrect credentials" in {
    val mockUserRepo = mock[MongoUserRepo]
    val sessionCacheProbe = TestProbe()
    val fakeBadCredentials = Credentials(userName = "john", password = "doe")

    when(mockUserRepo.getByCredentials(fakeBadCredentials)).thenReturn(None)

    Post(uri, fakeBadCredentials) ~> sessionsRoute(mock[ContextAuthenticator[Session]], mockUserRepo, sessionCacheProbe.ref) ~> check {
      rejection mustBe AuthenticationFailedRejection(CredentialsRejected, Nil)
    }

    sessionCacheProbe.expectNoMsg()
  }

  it must "allow a user to logout" in {
    val mockUserRepo = mock[MongoUserRepo]
    val sessionCacheProbe = TestProbe()
    val fakeSessionId = UUID.randomUUID
    val fakeSessionAuthenticator = TestUtils.fakeSessionAuthenticator(Map(fakeSessionId -> TestUtils.fakeUser()))

    Delete(uri + "me") ~> addHeader("session", fakeSessionId.toString) ~>
      sessionsRoute(fakeSessionAuthenticator, mockUserRepo, sessionCacheProbe.ref) ~> check {
        header[`Set-Cookie`].value mustBe `Set-Cookie`(HttpCookie(name = "session", content = "deleted", domain = Some("example.com"),
          path = Some("/"), expires = Some(SprayDateTime.MinValue)))
        status mustBe StatusCodes.OK
      }

    sessionCacheProbe.expectMsgPF() {
      case DeleteSession(sessionId) if sessionId == fakeSessionId ⇒
    }
  }

  it must "not find non-existent sessions" in {
    val mockUserRepo = mock[MongoUserRepo]
    val sessionCacheActor = TestActorRef(new SessionCacheActor(mockUserRepo, metrics = false))

    Get(uri + UUID.randomUUID) ~> sessionsRoute(mock[ContextAuthenticator[Session]], mockUserRepo, sessionCacheActor) ~> check {
      status mustBe StatusCodes.NotFound
    }
  }

}
