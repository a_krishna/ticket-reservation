package com.myticket.service.routes

import akka.testkit.TestProbe
import com.myticket.service.{ RouteSpec, TestUtils }
import com.myticket.service.actors.WebServiceActor
import com.myticket.service.domain._
import com.myticket.core.domain._
import com.myticket.core.repo.MongoUserRepo
import org.mockito.ArgumentCaptor
import org.mockito.Matchers.{ any, refEq, eq ⇒ mockEq }
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar
import play.api.libs.json.{ JsObject, JsString, Json }
import spray.http.{ ContentTypes, StatusCodes, Uri }
import spray.http.HttpHeaders.Location
import spray.httpx.PlayJsonSupport
import spray.routing.HttpService

import scala.collection.immutable.Seq
import java.util.UUID

import com.myticket.core.ObjectId
import com.myticket.service.routes.UsersRoute

//TODO: write some negative tests - particularly, failed credential update
class UsersRouteSpec extends RouteSpec with UsersRoute with PlayJsonSupport with MockitoSugar with HttpService {

  import WebServiceActor.exceptionHandler

  val uri = "/users/"
  override val actorRefFactory = system

  val nonAdminSessionId = UUID.randomUUID
  val adminSessionId = UUID.randomUUID
  val adminInAllOrgSessionId = UUID.randomUUID
  val fakeSessionAuthenticator = TestUtils.fakeSessionAuthenticator(
    Map(
      nonAdminSessionId -> TestUtils.fakeUser().copy(role = NormalUser),
      adminSessionId -> TestUtils.fakeUser(),
      adminInAllOrgSessionId -> TestUtils.fakeUser()))

  "Users route" must "get all users from all organizations if he is an admin in all organizations" in {
    val mockUserRepo = mock[MongoUserRepo]
    val fakeUser = TestUtils.fakeUser().copy(hashedPassword = "*****", salt = "*****")
    val userWithSomePermissions = fakeUser.copy(
      id = ObjectId())
    val userWithAllPermissions = fakeUser.copy(
      id = ObjectId())
    val users = Seq(fakeUser, userWithAllPermissions, userWithSomePermissions)

    when(mockUserRepo.getAll).thenReturn(users)

    Get(uri) ~> addHeader("session", adminSessionId.toString) ~>
      usersRoute(fakeSessionAuthenticator, mockUserRepo, TestProbe().ref) ~> check {
        responseAs[Seq[User]] mustBe users
        contentType mustBe ContentTypes.`application/json`
        status mustBe StatusCodes.OK
      }

    verify(mockUserRepo).getAll
  }

  it must "get all users in permitted organization if he is only an admin in some organizations" in {
    val mockUserRepo = mock[MongoUserRepo]
    val fakeUser = TestUtils.fakeUser().copy(hashedPassword = "*****", salt = "*****")
    val userWithSomePermissions = fakeUser.copy(
      id = ObjectId())
    val userWithAllPermissions = fakeUser.copy(
      id = ObjectId())
    val users = Seq(fakeUser, userWithAllPermissions, userWithSomePermissions)
    when(mockUserRepo.getAll).thenReturn(users)

    Get(uri) ~> addHeader("session", adminInAllOrgSessionId.toString) ~>
      usersRoute(fakeSessionAuthenticator, mockUserRepo, TestProbe().ref) ~> check {
        responseAs[Seq[User]] mustBe users
        contentType mustBe ContentTypes.`application/json`
        status mustBe StatusCodes.OK
      }

    verify(mockUserRepo).getAll
  }

  it must "reject a get request if the session user is not a user admin" in {
    val mockUserRepo = mock[MongoUserRepo]

    Get(uri) ~> addHeader("session", nonAdminSessionId.toString) ~>
      sealRoute(usersRoute(fakeSessionAuthenticator, mockUserRepo, TestProbe().ref)) ~> check {
        status mustBe StatusCodes.Forbidden
      }
  }

  it must "get a user" in {
    val mockUserRepo = mock[MongoUserRepo]
    val fakeUser = TestUtils.fakeUser()

    when(mockUserRepo.get(any[mockUserRepo.Id])).thenReturn(Some(fakeUser))

    Get(uri + fakeUser.id) ~> addHeader("session", adminInAllOrgSessionId.toString) ~>
      usersRoute(fakeSessionAuthenticator, mockUserRepo, TestProbe().ref) ~> check {

        responseAs[User] mustBe fakeUser.copy(hashedPassword = "*****", salt = "*****")
        contentType mustBe ContentTypes.`application/json`
        status mustBe StatusCodes.OK
      }

    verify(mockUserRepo).get(fakeUser.id)
  }

  it must "update credentials" in {
    val mockUserRepo = mock[MongoUserRepo]
    val fakeUserId = ObjectId()
    val fakeCredentials = Credentials(userName = "janedoe", password = "newpassword")
    implicit val credentialsWriter = Json.writes[Credentials] //This writer is needed only for this test, not production

    when(mockUserRepo.get(fakeUserId)).thenReturn(Some(TestUtils.fakeUser().copy(id = fakeUserId)))

    Post(uri + fakeUserId, fakeCredentials) ~> addHeader("session", adminInAllOrgSessionId.toString) ~>
      usersRoute(fakeSessionAuthenticator, mockUserRepo, TestProbe().ref) ~> check {
        status mustBe StatusCodes.OK
      }

    verify(mockUserRepo).get(fakeUserId) //TODO: look into making these get-followed-by-update actions atomic
    verify(mockUserRepo).updateCredentials(fakeUserId, fakeCredentials)
  }

  it must "update a user" in {
    val mockUserRepo = mock[MongoUserRepo]
    val fakeUserId = ObjectId()
    val fakePartialUser = PartialUser(name = Some("john doe"), updatedOn = None)
    val sessionCacheProbe = TestProbe()
    implicit val partialUserWriter = Json.writes[PartialUser] //This writer is needed only for this test, not production

    when(mockUserRepo.get(fakeUserId)).thenReturn(Some(TestUtils.fakeUser().copy(id = fakeUserId)))

    Post(uri + fakeUserId, fakePartialUser) ~> addHeader("session", adminInAllOrgSessionId.toString) ~>
      usersRoute(fakeSessionAuthenticator, mockUserRepo, sessionCacheProbe.ref) ~> check {
        status mustBe StatusCodes.OK
      }

    sessionCacheProbe.expectMsgPF() {
      case RefreshSessions(userId) if userId == fakeUserId ⇒
    }

    verify(mockUserRepo).get(fakeUserId)
    verify(mockUserRepo).update(mockEq(fakeUserId), refEq(fakePartialUser, "updatedOn"))
  }

  it must "delete a user" in {
    val mockUserRepo = mock[MongoUserRepo]
    val fakeUserId = ObjectId()
    val sessionCacheProbe = TestProbe()

    when(mockUserRepo.get(fakeUserId)).thenReturn(Some(TestUtils.fakeUser().copy(id = fakeUserId)))

    Delete(uri + fakeUserId) ~> addHeader("session", adminInAllOrgSessionId.toString) ~>
      usersRoute(fakeSessionAuthenticator, mockUserRepo, sessionCacheProbe.ref) ~> check {
        status mustBe StatusCodes.OK
      }

    sessionCacheProbe.expectMsgPF() {
      case RefreshSessions(userId) if userId == fakeUserId ⇒
    }

    verify(mockUserRepo).get(fakeUserId)
    verify(mockUserRepo).delete(fakeUserId)
  }

  it must "create a new user" in {
    val mockUserRepo = mock[MongoUserRepo]
    val fakePassword = "hunter222"
    val fakeUser = TestUtils.fakeUser(fakePassword)
    val dispatcherProbe = TestProbe()

    //Generate New User JSON to POST on this route
    //New User is the same as User except it requires an additional 'password' field. The credential generating reader
    //will create the 'id', 'hashedPassword', 'salt' and 'createdOn' fields
    val newUserJson = Json.toJson(fakeUser).as[JsObject] - "id" - "hashedPassword" - "salt" - "createdOn" +
      ("password" -> JsString(fakePassword))

    Post(uri + "create/", newUserJson) ~> addHeader("session", adminInAllOrgSessionId.toString) ~>
      usersRoute(fakeSessionAuthenticator, mockUserRepo, TestProbe().ref) ~> check {

        val captor = ArgumentCaptor.forClass(classOf[User])

        verify(mockUserRepo).insert(captor.capture())

        header[Location].value mustBe Location(Uri(Uri./ + "users/" + captor.getValue.id.toString))
        responseAs[JsObject] mustBe Json.obj("id" -> captor.getValue.id.toString)
        status mustBe StatusCodes.Created
      }

    verify(mockUserRepo).insert(refEq(fakeUser, "id", "hashedPassword", "salt", "createdOn"))
  }
}

