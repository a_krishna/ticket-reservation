package com.myticket.service.routes

import java.util.UUID

import com.myticket.core.domain._
import com.myticket.service.api.TheatreService
import com.myticket.service.domain.{ SeatSpecification, TheatreScreen }
import com.myticket.service.{ RouteSpec, TestUtils }
import org.mockito.Mockito.{ verify, when }
import org.scalatest.mock.MockitoSugar
import play.api.libs.json.Json
import spray.http.{ ContentTypes, StatusCodes }
import spray.httpx.PlayJsonSupport

import scala.concurrent.{ ExecutionContext, Future }
import scalaz.Scalaz._

class ScreenRouteSpec extends RouteSpec with ScreenRoute with PlayJsonSupport with MockitoSugar {
  import com.myticket.service.actors.WebServiceActor.exceptionHandler

  val uri = "/theatre/"

  val nonAdminSessionId = UUID.randomUUID
  val adminSessionId = UUID.randomUUID
  val adminInAllOrgSessionId = UUID.randomUUID
  val fakeSessionAuthenticator = TestUtils.fakeSessionAuthenticator(
    Map(
      nonAdminSessionId -> TestUtils.fakeUser().copy(role = NormalUser),
      adminSessionId -> TestUtils.fakeUser(),
      adminInAllOrgSessionId -> TestUtils.fakeUser()))

  "Users route" must "get all Theatres " in {
    val theatreService = mock[TheatreService]
    val fakeUser = TestUtils.fakeUser().copy(hashedPassword = "*****", salt = "*****")

    val theatres = List(Theatre(name = "xxx", city = "yy", state = "vv", country = "zz"))

    when(theatreService.getTheatre()).thenReturn(theatres)

    implicit val ec = ExecutionContext.Implicits
    Get(uri) ~> addHeader("session", adminSessionId.toString) ~>
      screenRoute(fakeSessionAuthenticator, theatreService) ~> check {
        responseAs[List[Theatre]] mustBe theatres
        contentType mustBe ContentTypes.`application/json`
        status mustBe StatusCodes.OK
      }

    verify(theatreService).getTheatre()
  }

  it must "Create Theatre with patial refrence" in {
    val theatreService = mock[TheatreService]
    val fakeUser = TestUtils.fakeUser().copy(hashedPassword = "*****", salt = "*****")

    val partialTheatre = PartialTheatre(name = Some("xxx"), city = Some("yy"), state = Some("dd"), country = Some("ee"))
    val json = Json.toJson(partialTheatre)

    when(theatreService.createTheatre(partialTheatre)).thenReturn(Future.successful("theater1".success))
    implicit val ec = ExecutionContext.Implicits
    Post(uri, json) ~> addHeader("session", adminSessionId.toString) ~>
      screenRoute(fakeSessionAuthenticator, theatreService) ~> check {
        responseAs[String] mustBe "theater1"
        status mustBe StatusCodes.Created
      }

    verify(theatreService).createTheatre(partialTheatre)
  }

  it must "Create Screen based on user prefrence screen" in {
    val theatreService = mock[TheatreService]
    val fakeUser = TestUtils.fakeUser().copy(hashedPassword = "*****", salt = "*****")

    val theaterScreen = TheatreScreen("xxxx", IMAX, List(SeatSpecification("GOLD", 150.0D, 3, 3)))
    val json = Json.toJson(theaterScreen)

    when(theatreService.createScreen(theaterScreen)).thenReturn(Future.successful("xxx".success))
    implicit val ec = ExecutionContext.Implicits
    Post(uri + "screen/", json) ~> addHeader("session", adminSessionId.toString) ~>
      screenRoute(fakeSessionAuthenticator, theatreService) ~> check {
        responseAs[String] mustBe "xxx"
        status mustBe StatusCodes.Created
      }

    verify(theatreService).createScreen(theaterScreen)
  }

  it must "add Screen based on ticket ID" in {
    val theatreService = mock[TheatreService]
    val fakeUser = TestUtils.fakeUser().copy(hashedPassword = "*****", salt = "*****")

    when(theatreService.addScreensToTheatre("1", Set("2"))).thenReturn(Future.successful(().successNel))
    implicit val ec = ExecutionContext.Implicits
    Get(uri + "screen?theatreId=1&screenId=2") ~> addHeader("session", adminSessionId.toString) ~>
      screenRoute(fakeSessionAuthenticator, theatreService) ~> check {
        status mustBe StatusCodes.OK
      }

    verify(theatreService).addScreensToTheatre("1", Set("2"))
  }

  it must "getTheatre By Name" in {
    val theatreService = mock[TheatreService]
    val fakeUser = TestUtils.fakeUser().copy(hashedPassword = "*****", salt = "*****")
    val theatre = Theatre(name = "xxx", city = "yy", state = "vv", country = "zz")
    when(theatreService.getByTheatreName("xxx")).thenReturn(Some(theatre))
    implicit val ec = ExecutionContext.Implicits
    Get(uri + "screen?theatreName=xxx") ~> addHeader("session", adminSessionId.toString) ~>
      screenRoute(fakeSessionAuthenticator, theatreService) ~> check {
        responseAs[Option[Theatre]] mustBe Some(theatre)
        contentType mustBe ContentTypes.`application/json`
        status mustBe StatusCodes.OK
      }

    verify(theatreService).getByTheatreName("xxx")
  }

  it must "getScreen By Name" in {
    val theatreService = mock[TheatreService]
    val fakeUser = TestUtils.fakeUser().copy(hashedPassword = "*****", salt = "*****")
    val theatre = Screen(screenName = "yyyy")
    when(theatreService.getByScreenName("yyyy")).thenReturn(Some(theatre))
    implicit val ec = ExecutionContext.Implicits
    Get(uri + "screen?screenName=yyyy") ~> addHeader("session", adminSessionId.toString) ~>
      screenRoute(fakeSessionAuthenticator, theatreService) ~> check {
        responseAs[Option[Screen]] mustBe Some(theatre)
        contentType mustBe ContentTypes.`application/json`
        status mustBe StatusCodes.OK
      }
    verify(theatreService).getByScreenName("yyyy")
  }

}
