import sbt._

object MyTicketBuild extends Plugin {

  object Versions {
    val Camel = "2.15.3"
    val CamelActiveMQ = "5.11.2"
    val Joda = "2.1"
    val JodaConvert = "1.3.1"
    val Netty = "3.10.4.Final"
    val PegDown = "1.5.0"
    val Casbah = "2.8.2"
    val CoreVersion = "3.0.10"
    val ScalaTest = "2.2.5"
    val Log4jOverSlf4j = "1.7.13"
    val Akka = "2.3.14"
    val Scalaz = "7.1.2"
    val Spray = "1.3.3"
    val Salat = "1.9.9"
    val ScalaLogging = "3.1.0"
    val PlayJson = "2.3.10"
    val PlayJsonVariants = "2.0"
    val Squants = "0.6.1-SNAPSHOT"
    val Logback = "1.1.3"
    val ScalaCheck = "1.12.2"
    val ScalaMock = "3.2.2"
    val EmbedMongo = "1.47.3"
    val LogstashEncoder = "4.4"
    val EmbeddedRedisVersion = "0.6"
    val RedisClientVersion   =   "3.0"
    val NScalaTime = "2.0.0"
    val Mockito = "1.10.19"
    val KamonVersion = "0.5.1"
    val AspectJVersion="1.8.7"
  }

  object Formatting {

    import com.typesafe.sbt.SbtScalariform._

    lazy val defaultSettings = scalariformSettings ++ Seq(
      ScalariformKeys.preferences in Compile := defaultPreferences,
      ScalariformKeys.preferences in Test := defaultPreferences)

    val defaultPreferences = {
      import scalariform.formatter.preferences._
      FormattingPreferences()
        .setPreference(AlignParameters, false)
        .setPreference(AlignSingleLineCaseStatements, true)
        .setPreference(CompactControlReadability, true)
        .setPreference(CompactStringConcatenation, false)
        .setPreference(DoubleIndentClassDeclaration, true)
        .setPreference(FormatXml, true)
        .setPreference(IndentLocalDefs, false)
        .setPreference(IndentPackageBlocks, true)
        .setPreference(IndentSpaces, 2)
        .setPreference(MultilineScaladocCommentsStartOnFirstLine, false)
        .setPreference(PreserveSpaceBeforeArguments, false)
        .setPreference(PreserveDanglingCloseParenthesis, false)
        .setPreference(RewriteArrowSymbols, true)
        .setPreference(SpaceBeforeColon, false)
        .setPreference(SpaceInsideBrackets, false)
        .setPreference(SpacesWithinPatternBinders, true)
    }
  }

  override def projectSettings: Seq[Def.Setting[_]] = Formatting.defaultSettings
}
