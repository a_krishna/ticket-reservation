addSbtPlugin("com.typesafe.sbt"   % "sbt-scalariform" % "1.3.0")

addSbtPlugin("com.eed3si9n"       % "sbt-assembly"    % "0.14.1")

addSbtPlugin("com.github.gseitz"  % "sbt-release"     % "1.0.0")

addSbtPlugin("com.eed3si9n"       % "sbt-buildinfo"   % "0.5.0")

addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.8.2")

addSbtPlugin("com.timushev.sbt"   % "sbt-updates"     % "0.1.10")

addSbtPlugin("com.typesafe.sbt"   % "sbt-aspectj"     % "0.9.4")

